﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Magenoco.Core.IO;

namespace Magenoco.Core
{
    public class Defaults
    {
        public static IFileSystem FileSystem { get; set; }
        public static ITemplateSource TemplateSource { get; set; }

        static Defaults()
        {
            FileSystem = new RealFileSystem();
        }
    }
}
