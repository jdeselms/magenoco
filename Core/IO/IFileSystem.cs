﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Magenoco.Core.IO
{
    public interface IFileSystem
    {
        void WriteFile(string path, string content);
        string ReadFile(string path);

        bool FileExists(string path);
        void DeleteFile(string path);

        void CreateDirectoryIfNotExists(string path);
    }
}
