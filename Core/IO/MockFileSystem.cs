﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Magenoco.Core.IO
{
    public class MockFileSystem : IFileSystem
    {
        private IDictionary<string, string> _files = new Dictionary<string,string>();

        public void WriteFile(string path, string contents)
        {
            _files[path.ToLower()] = contents;
        }

        public string ReadFile(string path)
        {
            string result;
            if (!_files.TryGetValue(path.ToLower(), out result))
            {
                throw new FileNotFoundException(path);
            }

            return result;
        }

        public bool FileExists(string path)
        {
            return _files.ContainsKey(path.ToLower());
        }

        public void DeleteFile(string path)
        {
            _files.Remove(path.ToLower());
        }

        public void CreateDirectoryIfNotExists(string path)
        {
        }

        public string CurrentDirectory { get; set; }
    }
}
