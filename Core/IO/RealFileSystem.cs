﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Magenoco.Core.IO
{
    public class RealFileSystem : IFileSystem
    {
        private readonly string _currentDirectory;
        public RealFileSystem(string currentDirectory=null)
        {
            _currentDirectory = currentDirectory ?? Environment.CurrentDirectory;
        }

        public void WriteFile(string path, string content)
        {
            string absolutePath = GetAbsolutePath(path);

            File.WriteAllText(absolutePath, content);
        }

        public string ReadFile(string path)
        {
            string absolutePath = GetAbsolutePath(path);

            return File.ReadAllText(absolutePath);
        }

        public bool FileExists(string path)
        {
            string absolutePath = GetAbsolutePath(path);
            return File.Exists(absolutePath);
        }

        public void DeleteFile(string path)
        {
            string absolutePath = GetAbsolutePath(path);
            File.Delete(absolutePath);
        }

        private string GetAbsolutePath(string path)
        {
            if (Path.GetFullPath(path) == path)
            {
                return path;
            }
            else
            {
                return Path.Combine(_currentDirectory, path);
            }
        }

        public void CreateDirectoryIfNotExists(string path)
        {
            Directory.CreateDirectory(path);
        }
    }
}
