﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Magenoco.Core.Text;
using Magenoco.Core.Processor;
using Magenoco.Core.IO;

namespace Magenoco.Core
{
    public interface ITemplateSource
    {
        Template GetTemplate(string name);
        CodeGenerator GetCodeGenerator(string templateName, string childName, ITokenSource tokenSource, IFileSystem fileSystem);
    }
}
