﻿using Magenoco.Core.IO;
using Magenoco.Core.Processor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Magenoco.Core
{
    public class MagenocoBuilder
    {
        private readonly TemplateSource _templateSource = new TemplateSource();
        private readonly IFileSystem _fileSystem;

        private readonly IList<string> _preprocessors = new List<string>();
        private readonly IList<string> _templatesToRun = new List<string>();
        private readonly IList<string> _postprocessors = new List<string>();
        
        public MagenocoBuilder(IFileSystem fileSystem = null)
        {
            _fileSystem = fileSystem ?? new RealFileSystem();
        }

        public void AddCodeGeneratorAssembly(Assembly assembly)
        {
            if (assembly == null) throw new ArgumentNullException("assembly");

            _templateSource.AddAssembly(assembly);
        }

        public void AddTemplates(string templateSourceCode)
        {
            if (templateSourceCode == null) throw new ArgumentNullException("templateSource");

            _templateSource.AddTemplatesFromText(templateSourceCode);
        }

        public void AddTemplatesFromFile(string templateSourceFilename)
        {
            if (templateSourceFilename == null) throw new ArgumentNullException("templateSourceFilename");

            string source = _fileSystem.ReadFile(templateSourceFilename);

            AddTemplates(source);
        }

        public void AddPreProcessor(string name)
        {
            _preprocessors.Add(name);
        }

        public void AddTemplateToRun(string name)
        {
            _templatesToRun.Add(name);
        }

        public void AddPostProcessor(string name)
        {
            _postprocessors.Add(name);
        }

        public void Run(ITokenSource tokenSource)
        {
            foreach (var preprocessor in _preprocessors)
            {
                var preproc = _templateSource.GetExecutor(preprocessor, tokenSource);
                preproc.Execute();
            }

            foreach (var templateToRun in _templatesToRun)
            {
                var template = _templateSource.GetTemplate(templateToRun);
                template.Replace(tokenSource, _templateSource, _fileSystem);
            }

            foreach (var postprocessor in _postprocessors)
            {
                var postproc = _templateSource.GetExecutor(postprocessor, tokenSource);
                postproc.Execute();
            }
        }
    }
}
