﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Magenoco.Core
{
    public class MagenocoException : Exception
    {
        public MagenocoException(string message=null, Exception innerException=null) : base(message, innerException)
        {
        }
    }
}
