﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Magenoco.Core.IO;
using Magenoco.Core.Text;

namespace Magenoco.Core.Processor
{
    public abstract class CSharpFileBuilder : PrettyPrintingFileBuilder
    {
        public CSharpFileBuilder(ITokenSource node, ITemplateSource templateSource, IFileSystem fileSystem=null) : base(node, new CSharpPrettyPrinter(), templateSource, fileSystem)
        {
        }
    }
}
