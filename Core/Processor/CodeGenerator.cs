﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using Magenoco.Core.IO;
using Magenoco.Core.Text;

namespace Magenoco.Core.Processor
{
    /// <summary>
    /// NodeGenerator that uses a template to generate code.
    /// </summary>
    public class CodeGenerator : NodeProcessor, ICodeGenerator
    {
        public CodeGenerator(ITokenSource node, ITemplateSource templateSource, IFileSystem fileSystem=null) : base(node, templateSource, fileSystem)
        {
        }

        public string GenerateCode(Template template)
        {
            return template.Replace(this, TemplateSource, FileSystem);
        }
    }
}
