﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using Magenoco.Core.Types;
using Magenoco.Core.IO;
using Magenoco.Core.Text;

namespace Magenoco.Core.Processor
{
    public abstract class FileBuilder : NodeExecutor
    {
        private readonly IFileSystem _fileSystem;

        public FileBuilder(ITokenSource node, ITemplateSource templateSource, IFileSystem fileSystem=null) : base(node, templateSource)
        {
            _fileSystem = fileSystem ?? Defaults.FileSystem;
        }

        protected abstract Template Template
        {
            get;
        }

        protected virtual string GenerateCode()
        {
            return Template.Replace(this, TemplateSource, null);
        }

        protected abstract string OutputPath { get; }

        public override void Execute()
        {
            var code = GenerateCode();

            _fileSystem.WriteFile(OutputPath, code);
        }
    }
}
