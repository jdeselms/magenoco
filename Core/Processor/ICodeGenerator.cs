﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Magenoco.Core.Text;

namespace Magenoco.Core.Processor
{
    public interface ICodeGenerator
    {
        string GenerateCode(Template template = null);
    }
}
