﻿using Magenoco.Core.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Magenoco.Core.Processor
{
    public interface ITokenSource
    {
        string Name { get; }

        ITokenSource ParentOrNull { get; }

        string GetAttribute(TextToken name);

        string GetAttribute(string name);

        bool TryGetAttribute(string name, out string value);

        IEnumerable<ITokenSource> GetChildren(params string[] names);

        IEnumerable<ITokenSource> GetAllChildren();

        IEnumerable<ICodeGenerator> GetCodeGeneratorsForChildren(string templateName, string[] childNames, ITemplateSource templateSource);

        IEnumerable<ICodeGenerator> TransformCodeGeneratorForChildToken(List<ICodeGenerator> childCodeGenerators, ChildToken childToken);
    }
}
