﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Magenoco.Core.Text;

namespace Magenoco.Core.Processor
{
    public class LiteralCode : ICodeGenerator
    {
        private readonly string _code;

        public LiteralCode(string code)
        {
            _code = code;
        }

        public string GenerateCode(Template template = null)
        {
            return _code;
        }
    }
}
