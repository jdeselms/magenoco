﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Magenoco.Core.Processor
{
    /// <summary>
    /// Represents a node processor that does something via its "Execute" method.
    /// </summary>
    public abstract class NodeExecutor : NodeProcessor
    {
        public NodeExecutor(ITokenSource node, ITemplateSource templateSource) : base(node, templateSource)
        {
        }

        public abstract void Execute();
    }
}
