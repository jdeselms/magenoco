﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml;
using Magenoco.Core.IO;
using Magenoco.Core.Text;

namespace Magenoco.Core.Processor
{
    /// <summary>
    /// Common base class for things that take Xml nodes and process them in some way.
    /// </summary>
    public class NodeProcessor : ITokenSource
    {
        private readonly ITokenSource _node;
        private readonly IFileSystem _fileSystem;
        private readonly ITemplateSource _templateSource;

        public NodeProcessor(ITokenSource node, ITemplateSource templateSource, IFileSystem fileSystem=null)
        {
            if (node == null)
            {
                throw new ArgumentNullException("node");
            }
            _node = node;
            _fileSystem = fileSystem ?? Defaults.FileSystem;
            _templateSource = templateSource ?? Defaults.TemplateSource;
        }

        protected ITemplateSource TemplateSource { get { return _templateSource; } }

        protected Template CreateTemplateFromResource(string resourceName=null)
        {
            resourceName = resourceName ?? GetDefaultResourceName();

            return _templateSource.GetTemplate(resourceName);
        }

        public ITokenSource ParentOrNull
        {
            get { return _node.ParentOrNull; }
        }

        private string GetDefaultResourceName()
        {
            return GetType().Namespace + ".Templates." + GetType().Name + ".template";
        }

        private string GetDefaultFileName()
        {
            return "Templates\\" + GetType().Name + ".template";
        }

        protected IFileSystem FileSystem { get { return _fileSystem; } }

        public string Name
        {
            get { return _node.Name; }
        }

        protected ITokenSource Node
        {
            get { return _node; }
        }

        public string GetNameOrNull(string attributeName, CaseOption changeCase = CaseOption.NoChange)
        {
            string name;
            
            if (TryGetAttribute(attributeName, out name))
            {
                return NameHelper.ChangeCase(name, changeCase);
            }
            else
            {
                return null;
            }
        }

        public virtual string Camel(string input)
        {
            return NameHelper.ChangeCase(input, CaseOption.CamelCase);
        }

        public virtual string Pascal(string input)
        {
            return NameHelper.ChangeCase(input, CaseOption.PascalCase);
        }

        public virtual string Lower(string input)
        {
            return NameHelper.ChangeCase(input, CaseOption.Lowercase);
        }

        public virtual string Upper(string input)
        {
            return NameHelper.ChangeCase(input, CaseOption.Uppercase);
        }

        public virtual IEnumerable<ICodeGenerator> Commas(IEnumerable<ICodeGenerator> input)
        {
            var inputAsArray = input.ToArray();
            for (int i = 0; i < inputAsArray.Length; i++)
            {
                yield return inputAsArray[i];

                if (i != inputAsArray.Length - 1)
                {
                    yield return new LiteralCode(", ");
                }
            }
        }

        public string GetName(string attributeName, CaseOption changeCase = CaseOption.NoChange)
        {
            return NameHelper.ChangeCase(GetAttribute(new TextToken(attributeName)), changeCase);
        }

        public string GetAttribute(string token)
        {
            return GetAttribute(new TextToken(token));
        }

        public string GetAttribute(TextToken textToken)
        {
            string result = GetAttributeOrDefault(textToken, null);
            if (result == null)
            {
                throw new MagenocoException(string.Format("Attribute {0} is not defined on Xml Element {1} or as a property {2}", textToken.Text, Node.Name, GetType().FullName));
            }
            return result;
        }

        public string GetAttributeOrDefault(TextToken token, string defaultValue)
        {
            string result;

            if (!TryGetAttribute(token.Text, out result))
            {
                result = defaultValue;
            }

            if (token.TransformationOrNull != null && result != null)
            {
                result = TransformString(result, token.TransformationOrNull);
            }

            return result;
        }

        public bool GetBoolOrDefault(string tokenName, bool defaultValue)
        {
            string boolAsString = GetAttributeOrDefault(new TextToken(tokenName), null);
            if (boolAsString == null)
            {
                return defaultValue;
            }
            else
            {
                return bool.Parse(boolAsString);
            }
        }

        private string TransformString(string stringToTransformation, string transformationName)
        {
            var methodInfo = GetType().GetMethod(transformationName, BindingFlags.Static | BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            if (methodInfo == null)
            {
                throw new MagenocoException(string.Format("Could not find method {0} on class {1}.", transformationName, GetType().FullName));
            }

            if (methodInfo.IsStatic)
            {
                return (string)methodInfo.Invoke(null, new object[1] { stringToTransformation });
            }
            else
            {
                return (string)methodInfo.Invoke(this, new object[1] { stringToTransformation });
            }
        }

        public bool TryGetAttribute(string attributeName, out string value)
        {
            if (!Node.TryGetAttribute(attributeName, out value))
            {
                value = GetPropertyValueFromClassOrNull(attributeName);
            }

            return value != null;
        }

        public string GenerateCodeForChildren(string elementName, Func<ITokenSource, ICodeGenerator> generatorFunction, string separator = null)
        {
            return GenerateCodeForChildren(elementName, generatorFunction, strings => StringJoiner.Join(strings, separator));
        }

        public string GenerateCodeForChildren(string elementName, Func<ITokenSource, ICodeGenerator> generatorFunction, Func<IEnumerable<string>, string> stringJoinFunction)
        {
            StringBuilder result = new StringBuilder("");

            List<string> childStrings = new List<string>();

            foreach (ITokenSource el in GetChildren(elementName))
            {
                var gen = generatorFunction(el);
                childStrings.Add(gen.GenerateCode());
            }

            return stringJoinFunction(childStrings);
        }

        public IEnumerable<NodeExecutor> CreateExecutorsForChildren(string elementName, Func<ITokenSource, NodeExecutor> generatorFunction)
        {
            foreach (ITokenSource el in GetChildren(elementName))
            {
                yield return generatorFunction(el);
            }
        }

        public int? GetIntAttributeOrNull(string attributeName)
        {
            return GetIntAttributeInternal(attributeName, allowNulls: true);
        }

        public int GetIntAttribute(string attributeName)
        {
// ReSharper disable PossibleInvalidOperationException
            return GetIntAttributeInternal(attributeName, allowNulls: false).Value;
// ReSharper restore PossibleInvalidOperationException
        }

        private int? GetIntAttributeInternal(string attributeName, bool allowNulls)
        {
            string asString;
            if (!TryGetAttribute(attributeName, out asString))
            {
                return null;
            }

            int result;
            if (!int.TryParse(asString, out result))
            {
                throw new MagenocoException(string.Format("Expected integer attribute for {0}/{1}, but found \"{2}\"", Node.Name, attributeName, asString));
            }

            return result;
        }

        private string GetPropertyValueFromClassOrNull(string propertyName)
        {
            var propInfo = GetType().GetProperty(propertyName, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);
            if (propInfo != null)
            {
                var getMethod = propInfo.GetGetMethod(true);
                if (getMethod != null)
                {
                    object result = getMethod.Invoke(this, Enumerable.Empty<object>().ToArray());
                    return result == null ? null : result.ToString();
                }
            }

            return null;
        }

        public IEnumerable<ITokenSource> GetChildren(params string[] names)
        {
            return _node.GetChildren(names);
        }

        public IEnumerable<ICodeGenerator> TransformCodeGeneratorForChildToken(List<ICodeGenerator> childCodeGenerators, ChildToken childToken)
        {
            if (childToken.TransformationOrNull == null)
            {
                // Nothing to transform. Just return the original source.
                return childCodeGenerators;
            }
            else
            {
                var transformation = childToken.TransformationOrNull;
                var methodInfo = GetType().GetMethod(transformation, BindingFlags.Static | BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                if (methodInfo == null)
                {
                    throw new MagenocoException(string.Format("Could not find method {0} on class {1}.", transformation, GetType().FullName));
                }

                if (methodInfo.IsStatic)
                {
                    return (IEnumerable<ICodeGenerator>)methodInfo.Invoke(null, new object[1] { childCodeGenerators });
                }
                else
                {
                    return (IEnumerable<ICodeGenerator>)methodInfo.Invoke(this, new object[1] { childCodeGenerators });
                }
            }
        }

        public IEnumerable<ICodeGenerator> GetCodeGeneratorsForChildren(string templateName, string[] childNames, ITemplateSource templateSource)
        {
            foreach (var child in GetChildren(childNames))
            {
                var codeGen = templateSource.GetCodeGenerator(templateName, child.Name, child, _fileSystem);
                yield return codeGen;
            }
        }

        private string GetDefaultChildToken()
        {
            var all = GetAllChildren().Distinct(new TokenNameComparer()).ToArray();

            if (all.Length == 0)
            {
                return "Foo";
            }

            if (all.Length > 1)
            {
                throw new MagenocoException("When specifying child tokens, you must specify the child name if there is more than one type of child.");
            }

            return all.First().Name;
        }

        private class TokenNameComparer : IEqualityComparer<ITokenSource>
        {
            public bool Equals(ITokenSource x, ITokenSource y)
            {
                return x.Name == y.Name;
            }

            public int GetHashCode(ITokenSource obj)
            {
                return obj.Name.GetHashCode();
            }
        }

        public IEnumerable<ITokenSource> GetAllChildren()
        {
            return _node.GetAllChildren();
        }
    }
}
