﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Magenoco.Core.IO;
using Magenoco.Core.Text;

namespace Magenoco.Core.Processor
{
    public abstract class PrettyPrintingFileBuilder : FileBuilder
    {
        private readonly IPrettyPrinter _prettyPrinter;

        public PrettyPrintingFileBuilder(ITokenSource node, IPrettyPrinter prettyPrinter, ITemplateSource templateSource, IFileSystem fileSystem) : base(node, templateSource, fileSystem)
        {
            _prettyPrinter = prettyPrinter;
        }

        protected override string GenerateCode()
        {
            string notPretty = base.GenerateCode();
            string pretty = _prettyPrinter.Prettify(notPretty);

            return pretty;
        }
    }
}
