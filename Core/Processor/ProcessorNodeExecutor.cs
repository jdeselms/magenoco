﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using Magenoco.Core.Types;
using Magenoco.Core.Text;

namespace Magenoco.Core.Processor
{
    /// <summary>
    /// Reads a "Processor" node and gets the class that it represents.
    /// </summary>
    public class ProcessorNodeExecutor : NodeExecutor
    {
        private readonly ITokenSource _inputFile;
        private readonly ITypeLoader _typeLoader;

        public ProcessorNodeExecutor(ITokenSource processorNode, ITokenSource inputFile, ITemplateSource templateSource, ITypeLoader typeLoader) : base(processorNode, templateSource)
        {
            if (inputFile == null) throw new ArgumentNullException("processorNode");
            if (typeLoader == null) throw new ArgumentNullException("typeLoader");
            _inputFile = inputFile;
            _typeLoader = typeLoader;
        }

        public override void Execute()
        {
            string assemblyNameAndType = GetAttribute(new TextToken("Code"));
            Type loadedType = _typeLoader.LoadType(assemblyNameAndType);
            if (!(typeof(NodeProcessor).IsAssignableFrom(loadedType)))
            {
                throw new MagenocoException(string.Format("Type {0} must be derived from NodeProcessor", loadedType.FullName));
            }

            var ctor = loadedType.GetConstructor(new [] {typeof (ITokenSource), typeof(ITemplateSource) });

            NodeExecutor executor = (NodeExecutor)ctor.Invoke(new object[] {_inputFile, TemplateSource });
            executor.Execute();
        }
    }
}
