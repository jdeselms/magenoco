﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using Magenoco.Core.Types;
using Magenoco.Core.IO;
using System.IO;
using System.Reflection;
using Magenoco.Core.Text;

namespace Magenoco.Core.Processor
{
    /// <summary>
    /// Node processor that accepts a system definition and executes a number of processors based on inputs from another source.
    /// </summary>
    public class ApplicationBuilder : NodeExecutor
    {
        private ITokenSource _inputFile;
        private ITypeLoader _typeLoader;

        public ApplicationBuilder(ITokenSource processorFile, ITokenSource inputFile, ITemplateSource templateSource, string defaultDirectory=null, ITypeLoader typeLoader=null) : base(processorFile, templateSource)
        {
            defaultDirectory = defaultDirectory ?? Environment.CurrentDirectory;

            _inputFile = inputFile;
            _typeLoader = typeLoader ?? new RealTypeLoader(defaultDirectory);
        }

        public static ApplicationBuilder CreateBuilderFromFiles(string processorFilename, string applicationFilename, IFileSystem fileSystem = null, ITypeLoader typeLoader = null)
        {
            fileSystem = fileSystem ?? new RealFileSystem();

            TemplateSource templateSource = new TemplateSource();

            var processorFile = fileSystem.ReadFile(processorFilename);
            var applicationFile = fileSystem.ReadFile(applicationFilename);

            string defaultDirectory = Path.GetDirectoryName(Path.GetFullPath(processorFilename));

            ITokenSource processorXml = LoadXml(processorFile);
            ITokenSource applicationXml = LoadXml(applicationFile);

            return new ApplicationBuilder(processorXml, applicationXml, templateSource, defaultDirectory, typeLoader);
        }

        public static void CreateBuilderFromResources(Assembly assemblyToLoadResourcesFrom, string processorResourceName, string applicationResourceName, IFileSystem fileSystem = null, ITypeLoader typeLoader = null)
        {
        }

        public override void Execute()
        {
            var executors = CreateExecutorsForChildren("Processor", p => new ProcessorNodeExecutor(p, _inputFile, TemplateSource, _typeLoader));

            foreach (var executor in executors)
            {
                executor.Execute();
            }
        }

        private static ITokenSource LoadXml(string xmlText)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmlText);

            return SimpleTokenSource.CreateFromXml(doc.DocumentElement, null);
        }
    }
}
