﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.IO;

namespace Magenoco.Core
{
    public class ResourceLoader
    {
        public string ReadResourceString(Assembly assembly, string resourceName)
        {
            string resource;
            if (TryReadResourceString(assembly, resourceName, out resource))
            {
                return resource;
            }
            else
            {
                throw new MagenocoException(string.Format("Resource {0} not found in assembly {1}", resourceName, assembly.FullName));
            }
        }

        public bool TryReadResourceString(Assembly assembly, string resourceName, out string resource)
        {
            var stream = assembly.GetManifestResourceStream(resourceName);
            if (stream != null)
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    resource = reader.ReadToEnd();
                    return true;
                }
            }
            else
            {
                resource = null;
                return false;
            }
        }

        public IEnumerable<string> GetTemplateResources(Assembly assembly)
        {
            foreach (string name in assembly.GetManifestResourceNames())
            {
                if (name.EndsWith(".template"))
                {
                    var stream = assembly.GetManifestResourceStream(name);
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        string resource = reader.ReadToEnd();
                        yield return resource;
                    }
                }
            }
        }

        public bool TryReadResourceStringPartial(Assembly assembly, string resourceName, out string resource)
        {
            foreach (string name in assembly.GetManifestResourceNames())
            {
                if (name.EndsWith(resourceName) || name.EndsWith(resourceName + ".template"))
                {
                    var stream = assembly.GetManifestResourceStream(name);
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        resource = reader.ReadToEnd();
                        return true;
                    }
                }
            }
            resource = null;
            return false;
        }
    }
}
