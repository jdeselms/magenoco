﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

using Magenoco.Core.Text;
using Magenoco.Core.Processor;
using Magenoco.Core.IO;

namespace Magenoco.Core
{
    public class TemplateSource : ITemplateSource
    {
        public TemplateSource(Type defaultCodeGeneratorType = null)
        {
            _defaultCodeGeneratorType = defaultCodeGeneratorType ?? typeof(CodeGenerator);
        }

        private readonly Type _defaultCodeGeneratorType;

        private List<Assembly> _assemblies = new List<Assembly>();
        private IDictionary<string, Template> _templates = new Dictionary<string, Template>();

        public void AddAssembly(Assembly assembly)
        {
            _assemblies.Add(assembly);
        }

        public void AddTemplatesFromText(string source)
        {
            foreach (var template in TemplateParser.Parse(source))
            {
                AddTemplate(template.Item1, template.Item2);
            }
        }

        public void AddTemplate(string name, Template template)
        {
            _templates.Add(name, template);
        }

        public Text.Template GetTemplate(string name)
        {
            if (_templates.ContainsKey(name))
            {
                return _templates[name];
            }

            ResourceLoader resourceLoader = new ResourceLoader();
            foreach (Assembly assembly in _assemblies)
            {
                string resource;

                if (resourceLoader.TryReadResourceStringPartial(assembly, name, out resource))
                {
                    return new Template(name, null, resource);
                }
            }

            throw new MagenocoException("Couldn't find template named " + name);
        }

        /// <summary>
        /// Given a number of templates in a single string, it parses them out.
        /// </summary>
        /// <param name="templateText"></param>
        public void ParseTemplates(string templateText)
        {
            foreach (var template in TemplateParser.Parse(templateText))
            {
                AddTemplate(template.Item1, template.Item2);
            }
        }

        public void ParseTemplatesFromResource(string resourceName)
        {
            ResourceLoader resourceLoader = new ResourceLoader();
            foreach (Assembly assembly in _assemblies)
            {
                string resource;

                if (resourceLoader.TryReadResourceStringPartial(assembly, resourceName, out resource))
                {
                    ParseTemplates(resource);
                }
            }
        }

        public NodeExecutor GetExecutor(string name, ITokenSource tokenSource)
        {
            foreach (var assembly in _assemblies)
            {
                foreach (var type in assembly.GetTypes())
                {
                    if (type.Name == name || type.Name == name)
                    {
                        var ctor = type.GetConstructor(new Type[] { typeof(ITokenSource), typeof(ITemplateSource) });
                        return (NodeExecutor)ctor.Invoke(new object[] { tokenSource, this });
                    }
                }
            }

            throw new MagenocoException(string.Format("Executor {0} not found.", name));
        }

        public Processor.CodeGenerator GetCodeGenerator(string templateName, string childName, ITokenSource tokenSource, IFileSystem fileSystem)
        {
            Type codeGeneratorType = _defaultCodeGeneratorType;

            foreach (var assembly in _assemblies)
            {
                foreach (var type in assembly.GetTypes())
                {
                    if (type.Name == templateName || type.Name == templateName + "CodeGenerator" || type.Name == childName || type.Name == childName + "CodeGenerator")
                    {
                        codeGeneratorType = type;
                        break;
                    }
                }
            }

            var ctor = codeGeneratorType.GetConstructor(new Type[] { typeof(ITokenSource), typeof(ITemplateSource), typeof(IFileSystem) });
            return (CodeGenerator)ctor.Invoke(new object[] { tokenSource, this, fileSystem });
        }
    }
}
