﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Magenoco.Core.Text
{
    public class CSharpPrettyPrinter : IPrettyPrinter
    {
        public string Prettify(string input)
        {
            StringBuilder result = new StringBuilder();
            int currentIndent = 0;

            foreach (string line in GetAllLines(input))
            {
                int effectiveIndent = currentIndent;
                if (line == "}")
                {
                    effectiveIndent -= 1;
                }

                result.AppendLine("".PadLeft(effectiveIndent * 4) + line);
                currentIndent += GetCurrentIndentationChange(line);
            }

            return result.ToString().Trim();
        }

        private static int GetCurrentIndentationChange(string line)
        {
            return line.Count(c => c == '{' || c == '[' || c == '(') - line.Count(c => c == '}' || c == ']' || c == ')');
        }

        private static IEnumerable<string> GetAllLinesUnfiltered(string input)
        {
            input = input.Replace("\r\n", "\n");
            input = input.Replace("\r", "\n");

            string[] lines = input.Split('\n');
            foreach (var line in lines)
            {
                foreach (var lineAfterCurlySplit in SplitLineForCurlyBrackets(line))
                {
                    yield return lineAfterCurlySplit.Trim();
                }
            }
        }

        private static IEnumerable<string> GetAllLines(string input)
        {
            bool lastWasBlank = false;
            bool lastWasOpenBrace = false;

            var allLines = GetAllLinesUnfiltered(input);

            List<string> linesIncludingBlanksBeforeBrace = new List<string>();

            foreach (string line in allLines)
            {
                bool ignoreLine = false;

                if (string.IsNullOrWhiteSpace(line))
                {
                    if (lastWasBlank || lastWasOpenBrace)
                    {
                        ignoreLine = true;
                    }
                    else
                    {
                        lastWasBlank = true;
                    }
                }
                else
                {
                    lastWasBlank = false;
                }

                if (!ignoreLine)
                {
                    linesIncludingBlanksBeforeBrace.Add(line);
                    lastWasOpenBrace = line == "{";
                }
            }

            return RemoveBlanksBeforeBraces(linesIncludingBlanksBeforeBrace);
        }

        private static IEnumerable<string> RemoveBlanksBeforeBraces(IEnumerable<string> lines)
        {
            List<string> linesInReverse = new List<string>();

            bool prevWasCurly = false;

            foreach (string line in lines.Reverse())
            {
                if (string.IsNullOrWhiteSpace(line) && prevWasCurly)
                {
                    continue;
                }

                linesInReverse.Add(line);

                prevWasCurly = line == "}";
            }

            return ((IEnumerable<string>)linesInReverse).Reverse();
        }

        private static IEnumerable<string> SplitLineForCurlyBrackets(string line)
        {
            line = line.Trim();

            int curlyIdx = line.IndexOf('{');
            if (curlyIdx == -1)
            {
                curlyIdx = line.IndexOf('}');
                if (curlyIdx == -1)
                {
                    yield return line;
                    yield break;
                }
            }

            if (curlyIdx > 0)
            {
                var substring = line.Substring(0, curlyIdx).Trim();
                foreach (var part in SplitLineForCurlyBrackets(substring))
                {
                    yield return part;
                }
            }
            yield return line.Substring(curlyIdx, 1);
            if (curlyIdx < line.Length)
            {
                var substring = line.Substring(curlyIdx + 1).Trim();
                foreach (var part in SplitLineForCurlyBrackets(substring))
                {
                    yield return part;
                }
            }
        }
    }
}
