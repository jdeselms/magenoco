﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Magenoco.Core.Text
{
    public enum CaseOption
    {
        NoChange,
        CamelCase,
        PascalCase,
        Uppercase,
        Lowercase,
    }
}
