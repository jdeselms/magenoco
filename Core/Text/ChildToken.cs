﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magenoco.Core.Text
{
    public class ChildToken : IComparable<ChildToken>
    {
        private readonly string _text;
        private readonly string _templateName;
        private readonly string _transformationOrNull;

        public ChildToken(string text)
        {
            _text = text;

            if (!_text.StartsWith("{{") || !_text.EndsWith("}}"))
            {
                ThrowFormatException();
            }

            string textWithTransformation = _text.Substring(2, _text.Length - 4);

            _templateName = StripTransformation(textWithTransformation, out _transformationOrNull);

            if (string.IsNullOrEmpty(_templateName))
            {
                ThrowFormatException();
            }
        }

        private string StripTransformation(string textWithTransformation, out string transformationOrNull)
        {
            string[] parts = textWithTransformation.Split('.');
            if (parts.Length == 1)
            {
                transformationOrNull = null;
                return textWithTransformation;
            }
            else
            {
                transformationOrNull = parts[1];
                return parts[0];
            }
        }

        public string TemplateName { get { return _templateName; } }
        
        public string TransformationOrNull { get { return _transformationOrNull; } }

        public override string ToString()
        {
            return _text;
        }

        private void ThrowFormatException()
        {
            throw new FormatException("Expected {{Template[;Processor]}}");
        }

        public int CompareTo(ChildToken obj)
        {
            int compare = _templateName.CompareTo(obj._templateName);
            if (compare == 0)
            {
                compare = (_transformationOrNull ?? string.Empty).CompareTo(obj._transformationOrNull ?? string.Empty);
            }

            return compare;
        }
    }
}
