﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Magenoco.Core.Text
{
    /// <summary>
    /// Takes some text and formats it so that it's purty.
    /// </summary>
    public interface IPrettyPrinter
    {
        string Prettify(string unprettySource);
    }
}
