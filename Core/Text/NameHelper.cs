﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Magenoco.Core.Text
{
    public class NameHelper
    {
        public static string ChangeCase(string s, CaseOption caseOption)
        {
            switch (caseOption)
            {
                case CaseOption.Lowercase: return ToLower(s);
                case CaseOption.Uppercase: return ToUpper(s);
                case CaseOption.PascalCase: return ToPascalCase(s);
                case CaseOption.CamelCase: return ToCamelCase(s);
                case CaseOption.NoChange: return s;
                default:
                    throw new ArgumentException("Unexpected option", "caseOption");
            }
        }
        /// <summary>
        /// Converts the string to "camel case", meaning that the first character is lowercase, and the remainder are either upper or lower.
        /// </summary>
        private static string ToCamelCase(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return s;
            }

            if (IsLowercaseWithUnderscores(s))
            {
                s = ConvertLowercaseWithUnderscoresToCamel(s);
            }

            if (Char.IsUpper(s[0]))
            {
                return Char.ToLower(s[0]) + s.Substring(1);
            }
            else
            {
                return s;
            }
        }

        private static string ConvertLowercaseWithUnderscoresToCamel(string s)
        {
            string result = "";
            bool capitalizeNext = false;

            foreach (char c in s)
            {
                if (c == '_')
                {
                    capitalizeNext = true;
                    continue;
                }

                if (capitalizeNext)
                {
                    result += Char.ToUpper(c);
                }
                else
                {
                    result += c;
                }

                capitalizeNext = false;
            }

            return result;
        }

        private static bool IsLowercaseWithUnderscores(string s)
        {
            bool hasUnderscores = false;
            bool hasUppercase = false;

            foreach (char c in s)
            {
                if (c == '_') hasUnderscores = true;
                if (Char.IsUpper(c)) hasUppercase = true;
            }

            return hasUnderscores && !hasUppercase;
        }

        /// <summary>
        /// Converts the string to "camel case", meaning that the first character is uppercase, and the remainder are either upper or lower.
        /// </summary>
        private static string ToPascalCase(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return s;
            }

            if (IsLowercaseWithUnderscores(s))
            {
                s = ConvertLowercaseWithUnderscoresToCamel(s);
            }

            if (Char.IsLower(s[0]))
            {
                return Char.ToUpper(s[0]) + s.Substring(1);
            }
            else
            {
                return s;
            }
        }

        /// <summary>
        /// Takes a pascal/camel case string and converts the words to uppercase, separated by "_"
        /// </summary>
        private static string ToUpper(string s)
        {
            List<int> wordStarts = new List<int>();
            for (int i = 1; i < s.Length; i++)
            {
                if (char.IsUpper(s[i]))
                {
                    wordStarts.Add(i);
                }
            }

            List<string> parts = new List<string>();
            int currStart = 0;

            foreach (int wordStart in wordStarts)
            {
                parts.Add(s.Substring(currStart, wordStart - currStart).ToUpper());
                currStart = wordStart;
            }

            if (currStart != s.Length)
            {
                parts.Add(s.Substring(currStart).ToUpper());
            }

            return string.Join("_", parts);
        }

        private static string ToLower(string s)
        {
            return ToUpper(s).ToLower();
        }
    }
}
