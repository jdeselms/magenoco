﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Magenoco.Core.Processor;

namespace Magenoco.Core.Text
{
    public class SimpleTokenSource : ITokenSource
    {
        private readonly IDictionary<string, string> _dict = new Dictionary<string, string>();
        private readonly IDictionary<string, IList<ITokenSource>> _children = new Dictionary<string, IList<ITokenSource>>();
        private readonly string _name;
        private readonly ITokenSource _parentOrNull;

        public SimpleTokenSource(string name, ITokenSource parentOrNull = null)
        {
            _name = name;
            _parentOrNull = parentOrNull;
        }

        public string Name { get { return _name; } }

        public string GetAttribute(string token)
        {
            return GetAttribute(new TextToken(token));
        }

        public string GetAttribute(TextToken token)
        {
            string result;

            if (!TryGetAttribute(token.Text, out result))
            {
                throw new MagenocoException("Couldn't find token " + token);
            }

            return result;
        }

        public bool TryGetAttribute(string token, out string value)
        {
            return _dict.TryGetValue(token.ToLower(), out value);
        }

        
        public static ITokenSource CreateFromXmlFile(string xmlFilename)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(xmlFilename);

            return CreateFromXml(doc.DocumentElement, null);
        }

        public ITokenSource ParentOrNull { get { return _parentOrNull; } }

        public static ITokenSource CreateFromXml(string xml)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);

            return CreateFromXml(doc.DocumentElement, null);
        }
        
        public static ITokenSource CreateFromXml(XmlNode xml, ITokenSource parent)
        {
            SimpleTokenSource tokenSource = new SimpleTokenSource(xml.Name, parent);
            foreach (XmlAttribute attr in xml.Attributes)
            {
                tokenSource.AddTextToken(attr.Name, attr.Value);
            }

            foreach (XmlNode child in xml.ChildNodes)
            {
                if (child is XmlElement)
                {
                    tokenSource.AddChild(CreateFromXml(child, tokenSource));
                }
            }

            return tokenSource;
        }

        public void AddTextToken(string token, string value)
        {
            _dict.Add(token.ToLower(), value);
        }

        public IEnumerable<ITokenSource> GetChildren(params string[] names)
        {
            // Wildcard means just take them all.
            if (names.Contains("*"))
            {
                foreach (var child in _children.SelectMany(c => c.Value))
                {
                    yield return child;
                }
                yield break;
            }

            IList<ITokenSource> children;
            foreach (string name in names)
            {
                if (_children.TryGetValue(name, out children))
                {
                    foreach (var child in children)
                    {
                        yield return child;
                    }
                }
            }
        }

        public void AddChild(ITokenSource child)
        {
            IList<ITokenSource> children;
            if (!_children.TryGetValue(child.Name, out children))
            {
                children = new List<ITokenSource>();
                _children[child.Name] = children;
            }
            children.Add(child);
        }

        public IEnumerable<ICodeGenerator> TransformCodeGeneratorForChildToken(List<ICodeGenerator> childCodeGenerators, ChildToken childToken)
        {
            return childCodeGenerators;
        }

        public IEnumerable<ICodeGenerator> GetCodeGeneratorsForChildren(string templateName, string[] childNames, ITemplateSource templateSource)
        {
            foreach (var child in GetChildren(childNames))
            {
                var codeGen = templateSource.GetCodeGenerator(templateName, child.Name, child, Defaults.FileSystem);
                yield return codeGen;
            }
        }

        public IEnumerable<ITokenSource> GetAllChildren()
        {
            return _children.Values.SelectMany(n => n);
        }
    }
}
