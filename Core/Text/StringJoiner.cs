﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Magenoco.Core.Text
{
    public class StringJoiner
    {
        /// <summary>
        /// If parts is not empty, then the returned string is prefixed.
        /// This is a shorthand that can be used for something like this:
        /// "class Foo : This, That"
        /// You'd use this: StringJoiner.JoinWithPrefixIfNotEmpty(" : ", classes, ", ");
        /// </summary>
        public static string JoinWithPrefixIfNotEmpty(string prefix, IEnumerable<string> parts, string delimeter=null)
        {
            var partsAsArray = parts.ToArray();

            if (partsAsArray.Length == 0)
            {
                return string.Empty;
            }
            else
            {
                return prefix + Join(parts, delimeter);
            }
        }

        /// <summary>
        /// Like JoinWithPrefixIfNotEmpty, allows a suffix to be added too.
        /// Useful for constructs like optional arg lists:
        /// For "(this, this)"
        /// you'd use: StringJoiner.JoinWithPrefixAndSuffixIfNotEmpty("(", ")", classes, ", ");
        /// </summary>
        public static string JoinWithPrefixAndSuffixIfNotEmpty(string prefix, string suffix, IEnumerable<string> parts, string delimeter=null)
        {
            var partsAsArray = parts.ToArray();

            if (partsAsArray.Length == 0)
            {
                return string.Empty;
            }
            else
            {
                return JoinWithPrefixIfNotEmpty(prefix, parts, delimeter) + suffix;
            }
        }

        /// <summary>
        /// Joins a number of strings, inserting a delimeter between each one.
        /// If the delimiter is not supplied, a couple things can happen:
        /// 
        /// 1) If the first string specified ends with a new line, then a new line delimiter is used.
        /// 2) If the first string does not end with a new line, then no delimieter is used.
        /// </summary>
        /// <param name="parts"></param>
        /// <param name="delimeter"></param>
        /// <returns></returns>
        public static string Join(IEnumerable<string> parts, string delimeter=null)
        {
            // If they don't specify a delimeter, then we infer the delimeter, and we 
            // trim the ending new lines from the parts.
            if (delimeter == null)
            {
                delimeter = delimeter ?? InferDelimeter(parts);
                parts = parts.Select(RemoveTrailingNewLine);
            }

            return string.Join(delimeter, parts);
        }

        private static string RemoveTrailingNewLine(string s)
        {
            return s.TrimEnd('\r', '\n');
        }

        private static string InferDelimeter(IEnumerable<string> parts)
        {
            string first = parts.FirstOrDefault();
            
            if (first == null)
                return string.Empty;

            // Does it end with a new line? Then that's the delimiter.
            if (first.EndsWith("\n") || first.EndsWith("\r") || first.EndsWith("\r\n"))
            {
                return "\r\n";
            }

            // No delimeter.
            return String.Empty;
        }
    }
}
