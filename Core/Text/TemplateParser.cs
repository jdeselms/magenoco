﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magenoco.Core.Text
{
    public class TemplateParser
    {
        public static IEnumerable<Tuple<string, Template>> Parse(string templateSource)
        {
            templateSource = templateSource.Replace("\r\n", "\n");
            string[] lines = templateSource.Split('\n');

            string currentTemplate = null;
            string currentAppliesTo = null;
            Template fileTemplate = null;
            StringBuilder templateText = new StringBuilder();

            foreach (var line in lines)
            {
                if (line.StartsWith("template"))
                {
                    if (currentTemplate != null)
                    {
                        yield return Tuple.Create(currentTemplate, new Template(currentTemplate, currentAppliesTo, templateText.ToString(), fileTemplate));
                        templateText = new StringBuilder();
                    }

                    currentTemplate = GetTemplateName(line);
                    currentAppliesTo = GetAppliesTo(line);
                    fileTemplate = GetFilenameTemplateIfSpecifiedOrNull(line);
                }
                else if (line.StartsWith("END"))
                {
                    if (currentTemplate != null)
                    {
                        yield return Tuple.Create(currentTemplate, new Template(currentTemplate, currentAppliesTo, templateText.ToString(), fileTemplate));
                        templateText = new StringBuilder();
                        currentTemplate = null;
                        currentAppliesTo = null;
                    }
                }
                else
                {
                    if (currentTemplate != null)
                    {
                        if (templateText.Length > 0)
                        {
                            templateText.Append("\n");
                        }
                        templateText.Append(line);
                    }
                }
            }

            if (currentTemplate != null)
            {
                yield return Tuple.Create(currentTemplate, new Template(currentTemplate, currentAppliesTo, templateText.ToString().TrimEnd(), fileTemplate));
            }
        }

        private static string GetTemplateName(string inputLine)
        {
            string nameAndAppliesTo = GetTemplateNameAndAppliesTo(inputLine);
            int parenIdx = nameAndAppliesTo.IndexOf('(');
            return nameAndAppliesTo.Substring(0, parenIdx);
        }

        private static string GetAppliesTo(string inputLine)
        {
            string nameAndAppliesTo = GetTemplateNameAndAppliesTo(inputLine);
            int parenIdx = nameAndAppliesTo.IndexOf('(');
            return nameAndAppliesTo.Substring(parenIdx + 1, nameAndAppliesTo.Length - parenIdx - 2);
        }

        private static string GetTemplateNameAndAppliesTo(string inputLine)
        {
            return GetTextAfterTemplateKeyword(inputLine)[0];
        }

        private static Template GetFilenameTemplateIfSpecifiedOrNull(string inputLine)
        {
            string[] textAfterTemplate = GetTextAfterTemplateKeyword(inputLine);
            if (textAfterTemplate.Length >= 2)
            {
                return new Template("", null, textAfterTemplate[1], null);
            }
            else
            {
                return null;
            }
        }

        private static string[] GetTextAfterTemplateKeyword(string inputLine)
        {
            return inputLine.Substring("template".Length).Trim().Split(' ');
        }
    }
}
