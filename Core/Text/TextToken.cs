﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magenoco.Core.Text
{
    public class TextToken
    {
        private readonly string _text;
        private readonly string _transformationOrNull;

        public TextToken(string text)
        {
            string[] parts = text.Split('.');
            _text = parts[0];
            _transformationOrNull = parts.Length > 1 ? parts[1] : null;
        }

        public string Text { get { return _text; } }

        public string TransformationOrNull { get { return _transformationOrNull; } }

        public override bool Equals(object obj)
        {
            if (obj == null) 
                return false;

            var otherToken = obj as TextToken;
            if (otherToken == null) 
                return false;

            return ToString() == otherToken.ToString();
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }

        public override string ToString()
        {
            return _text + (_transformationOrNull == null ? string.Empty : "." + _transformationOrNull);
        }
    }
}
