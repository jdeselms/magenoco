﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Magenoco.Core.Types
{
    public interface ITypeLoader
    {
        Type LoadType(string assemblyAndTypeName);
    }
}
