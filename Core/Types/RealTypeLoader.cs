﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Magenoco.Core.Types
{
    public class RealTypeLoader : ITypeLoader
    {
        private readonly string _defaultDirectory;
        public RealTypeLoader(string defaultDirectory)
        {
            _defaultDirectory = defaultDirectory;
        }

        public Type LoadType(string assemblyAndTypeName)
        {
            TypeSpec typeSpec = new TypeSpec(assemblyAndTypeName, _defaultDirectory);

            Assembly assembly;
            try
            {
                assembly = Assembly.LoadFile(typeSpec.AssemblyName);
            }
            catch (FileNotFoundException)
            {
                throw new MagenocoException(string.Format("Assembly file '{0}' cannot be found", typeSpec.AssemblyName));
            }

            var type = assembly.GetType(typeSpec.TypeName);
            if (type == null)
            {
                throw new MagenocoException(string.Format("Type '{0}' cannot be found in assembly '{1}'", typeSpec.TypeName, assembly.FullName));
            }

            return type;
        }
    }
}
