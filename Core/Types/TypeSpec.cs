﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Magenoco.Core.Types
{
    public class TypeSpec
    {
        private readonly string _assemblyName;
        private readonly string _typeName;

        public TypeSpec(string typeSpecString, string defaultDirectory)
        {
            string[] parts = typeSpecString.Split(',');
            if (parts.Length != 2)
            {
                throw new MagenocoException(string.Format("Type specification in wrong format. Must be 'assemblyName,typeName': {0}", typeSpecString));
            }

            if (string.IsNullOrWhiteSpace(parts[0]) || string.IsNullOrWhiteSpace(parts[1]))
            {
                throw new MagenocoException(string.Format("Type specification in wrong format. Must be 'assemblyName,typeName': {0}", typeSpecString));
            }

            _assemblyName = Path.Combine(defaultDirectory, parts[0].Trim());
            _typeName = parts[1].Trim();
        }

        public string AssemblyName { get { return _assemblyName; } }
        public string TypeName { get { return _typeName; } }
    }
}
