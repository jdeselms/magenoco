﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Magenoco.Core.Text;
using Magenoco.Core.Processor;

namespace Magenoco.Core.UnitTests
{
    [TestClass]
    public class ChildTokenTests
    {
        [TestMethod]
        public void TemplateName_SimpleChildToken()
        {
            ChildToken token = new ChildToken("{{Hello}}");
            Assert.AreEqual("Hello", token.TemplateName);
        }

        private ITokenSource CreateTokenSourceWithChild(string childName)
        {
            SimpleTokenSource parent = new SimpleTokenSource("parent");
            SimpleTokenSource child = new SimpleTokenSource(childName);
            parent.AddChild(child);

            return parent;
        }

        [TestMethod]
        public void ToString_ReturnsInputVerbatim()
        {
            ChildToken token = new ChildToken("{{ThisIsMyToken}}");
            Assert.AreEqual("{{ThisIsMyToken}}", token.ToString());
        }

        [TestMethod]
        [ExpectedException(typeof(FormatException))]
        public void Ctor_NoBrackets_FormatException()
        {
            ChildToken token = new ChildToken("missing brackets");
        }

        [TestMethod]
        public void TransformationOrNull_NoTransformation_Null()
        {
            ChildToken token = new ChildToken("{{Hello}}");
            Assert.IsNull(token.TransformationOrNull);
        }

        [TestMethod]
        public void TransformationOrNull_HasTransformation_ReturnsTransformation()
        {
            ChildToken token = new ChildToken("{{Hello.Commas}}");
            Assert.AreEqual("Commas", token.TransformationOrNull);
        }

        [TestMethod]
        public void ToString_WithTransformation_IncludesTransformation()
        {
            ChildToken token = new ChildToken("{{Hello.Commas}}");
            Assert.AreEqual("{{Hello.Commas}}", token.ToString());
        }

        [TestMethod]
        [ExpectedException(typeof(FormatException))]
        public void Ctor_NothingInside_FormatException()
        {
            ChildToken token = new ChildToken("{{}}");
        }
    }
}
