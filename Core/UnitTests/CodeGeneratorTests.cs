﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Magenoco.Core.IO;
using Magenoco.Core.Processor;
using Magenoco.Core.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Magenoco.Core.UnitTests
{
    [TestClass]
    public class CodeGeneratorTests
    {
        [TestMethod]
        public void CodeGenerator_UsingPrettyPrinter_PrettyPrints()
        {
            var prettyPrinter = new CSharpPrettyPrinter();

            var fileSystem = new MockFileSystem();

            PrettyPrintingFileBuilder gen = new TestPrettyPrintingFileBuilder(SimpleTokenSource.CreateFromXml("<foo/>"), prettyPrinter, TemplateTests.CreateTemplateSource(), fileSystem);
            gen.Execute();

            string file = fileSystem.ReadFile(@"c:\temp.cs");
            Assert.AreEqual("foo\r\n{\r\n}", file);
        }

        private class TestPrettyPrintingFileBuilder : PrettyPrintingFileBuilder
        {
            public TestPrettyPrintingFileBuilder(ITokenSource node, IPrettyPrinter prettyPrinter, ITemplateSource templateSource, IFileSystem fileSystem) : base(node, prettyPrinter, templateSource, fileSystem)
            {
            }

            protected override Template Template
            {
                get
                {
                    return new Template("_", "", "foo { }");
                }
            }

            protected override string OutputPath
            {
                get { return @"c:\temp.cs"; }
            }
        }
    }
}
