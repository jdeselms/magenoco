﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Magenoco.Core.Types;

namespace Magenoco.Core.UnitTests
{
    public class MockTypeLoader : ITypeLoader
    {
        private readonly IDictionary<string, IDictionary<string, Type>> _types = new Dictionary<string, IDictionary<string, Type>>();

        public void AddType(string assemblyAndTypeName, Type type)
        {
            TypeSpec typeSpec = new TypeSpec(assemblyAndTypeName, ".");
            if (!_types.ContainsKey(typeSpec.AssemblyName))
            {
                _types[typeSpec.AssemblyName] = new Dictionary<string, Type>();
            }

            _types[typeSpec.AssemblyName][typeSpec.TypeName] = type;
        }

        public Type LoadType(string assemblyAndTypeName)
        {
            // Just do this for its validation.
            TypeSpec typeSpec = new TypeSpec(assemblyAndTypeName, ".");
            return _types[typeSpec.AssemblyName][typeSpec.TypeName];
        }
    }
}
