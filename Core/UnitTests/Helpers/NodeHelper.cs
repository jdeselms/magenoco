﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using Magenoco.Core;
using Magenoco.Core.Processor;
using Magenoco.Core.Text;
using System.Reflection;

namespace Magenoco.Core.UnitTests
{
    public class NodeHelper : NodeProcessor
    {
        public static NodeHelper Create(string xml)
        {
            TemplateSource templateSource = new TemplateSource();
            templateSource.AddAssembly(Assembly.GetExecutingAssembly());
            return new NodeHelper(SimpleTokenSource.CreateFromXml(xml), templateSource);
        }

        public static XmlNode CreateDocument(string xml)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            return doc.DocumentElement;
        }

        public static NodeHelper CreateWithNullXml(ITemplateSource templateSource)
        {
            return new NodeHelper(null, templateSource);
        }

        public NodeHelper(ITokenSource node, ITemplateSource templateSource)
            : base(node, templateSource)
        {
        }

        private string Interjection { get { return "Yay"; } }

        public string StringThatLooksLikeInt { get { return "123"; } }

        public static int IntProperty { get { return 456; } }
    }
}
