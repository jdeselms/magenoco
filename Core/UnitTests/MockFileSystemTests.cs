﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Magenoco.Core.IO;

namespace Magenoco.Core.UnitTests
{
    [TestClass]
    public class MockFileSystemTests
    {
        [TestMethod]
        public void ReadFile_FileExists_ReadsFile()
        {
            IFileSystem fileSystem = new MockFileSystem();
            fileSystem.WriteFile("C:\\temp.txt", "Hello world");

            Assert.AreEqual("Hello world", fileSystem.ReadFile("C:\\temp.txt"));
        }

        [TestMethod]
        [ExpectedException(typeof(FileNotFoundException))]
        public void ReadFile_FileDoesntExist_Throws()
        {
            IFileSystem fileSystem = new MockFileSystem();
            fileSystem.ReadFile("bogus");
        }

        [TestMethod]
        public void FileExists_FileExists_True()
        {
            IFileSystem fileSystem = new MockFileSystem();
            fileSystem.WriteFile("temp.txt", "hi");
            Assert.IsTrue(fileSystem.FileExists("temp.txt"));
        }

        [TestMethod]
        public void FileExists_FileDoesntExist_False()
        {
            IFileSystem fileSystem = new MockFileSystem();
            Assert.IsFalse(fileSystem.FileExists("temp.txt"));
        }

        [TestMethod]
        public void DeleteFile_FileExists_FileDeleted()
        {
            IFileSystem fileSystem = new MockFileSystem();
            fileSystem.WriteFile("a.txt", "hi");

            fileSystem.DeleteFile("a.txt");

            Assert.IsFalse(fileSystem.FileExists("a.txt"));
        }

        [TestMethod]
        public void DeleteFile_FileDoesntExist_NoError()
        {
            IFileSystem fileSystem = new MockFileSystem();
            fileSystem.DeleteFile("foo.txt");
        }

        [TestMethod]
        public void FileExists_WhenCaseDoesntMatch_True()
        {
            IFileSystem fileSystem = new MockFileSystem();
            fileSystem.WriteFile("test.txt", "hi");

            Assert.IsTrue(fileSystem.FileExists("TEST.TXT"));
        }
    }
}
