﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Magenoco.Core.Text;

namespace Magenoco.Core.UnitTests
{
    [TestClass]
    public class NameHelperTests
    {
        [TestMethod]
        public void ToCamelCase_AlreadyInCamelCase_DoesNothing()
        {
            Assert.AreEqual("camelCase", NameHelper.ChangeCase("camelCase", CaseOption.CamelCase));
        }

        [TestMethod]
        public void ToCamelCase_WhenInPascalCase_Converts()
        {
            Assert.AreEqual("camelCase", NameHelper.ChangeCase("CamelCase", CaseOption.CamelCase));
        }

        [TestMethod]
        public void ToPascalCase_AlreadyInCamelCase_DoesNothing()
        {
            Assert.AreEqual("PascalCase", NameHelper.ChangeCase("PascalCase", CaseOption.PascalCase));
        }

        [TestMethod]
        public void ToPascalCase_WhenInCamelCase_Converts()
        {
            Assert.AreEqual("PascalCase", NameHelper.ChangeCase("pascalCase", CaseOption.PascalCase));
        }

        [TestMethod]
        public void ToPascalCase_SingleCharacter_Succeeds()
        {
            Assert.AreEqual("P", NameHelper.ChangeCase("p", CaseOption.PascalCase));
        }

        [TestMethod]
        public void ToUpper_SeveralWords_Success()
        {
            Assert.AreEqual("THIS_IS_A_TEST", NameHelper.ChangeCase("thisIsATest", CaseOption.Uppercase));
        }

        [TestMethod]
        public void ToUpper_SingleWord_Success()
        {
            Assert.AreEqual("HELLO", NameHelper.ChangeCase("hello", CaseOption.Uppercase));
        }

        [TestMethod]
        public void ToLower_SeveralWords_Success()
        {
            Assert.AreEqual("this_is_a_test", NameHelper.ChangeCase("thisIsATest", CaseOption.Lowercase));
        }

        [TestMethod]
        public void ToPascal_LowercaseWithUnderscore_Success()
        {
            Assert.AreEqual("ThisIsATest", NameHelper.ChangeCase("this_is_a_test", CaseOption.PascalCase));
        }

        [TestMethod]
        public void ToCamel_LowercaseWithUnderscore_Success()
        {
            Assert.AreEqual("thisIsATest", NameHelper.ChangeCase("this_is_a_test", CaseOption.CamelCase));
        }

        [TestMethod]
        public void ToLower_SingleWord_Success()
        {
            Assert.AreEqual("hello", NameHelper.ChangeCase("hello", CaseOption.Lowercase));
        }

    }
}
