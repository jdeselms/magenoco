﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Magenoco.Core;
using Magenoco.Core.Text;

namespace Magenoco.Core.UnitTests
{
    [TestClass]
    public class NodeProcessorTests
    {
        [TestMethod]
        public void GetStringAttribute_ThatExists_ReturnsValue()
        {
            var helper = NodeHelper.Create("<Application Attr=\"hello\" />");

            Assert.AreEqual("hello", helper.GetAttribute(new TextToken("Attr")));
        }

        [TestMethod]
        [ExpectedException(typeof(MagenocoException))]
        public void GetTextAttribute_ThatDoesntExists_Throws()
        {
            var helper = NodeHelper.Create("<Application Attr=\"hello\" />");

            helper.GetName("Bogus");
        }

        [TestMethod]
        public void GetStringAttributeOrNull_ThatExists_ReturnsValue()
        {
            var helper = NodeHelper.Create("<Application Attr=\"hello\" />");

            string text;
            helper.TryGetAttribute("Attr", out text);

            Assert.AreEqual("hello", text);
        }

        [TestMethod]
        public void GetStringAttributeOrNull_ThatDoesntExists_ReturnsFalse()
        {
            var helper = NodeHelper.Create("<Application Attr=\"hello\" />");

            string text;
            
            Assert.IsFalse(helper.TryGetAttribute("Bogus", out text));
        }

        [TestMethod]
        public void GetIntAttribute_ThatExists_ReturnsValue()
        {
            var helper = NodeHelper.Create("<Application Number=\"123\" />");

            Assert.AreEqual(123, helper.GetIntAttributeOrNull("Number"));
        }

        [TestMethod]
        public void GetIntAttributeOrNull_ThatDoesntExist_ReturnsNull()
        {
            var helper = NodeHelper.Create("<Application Number=\"123\" />");

            Assert.IsNull(helper.GetIntAttributeOrNull("Bogus"));
        }

        [TestMethod]
        public void GetIntAttributeOrNull_ThatExists_ReturnsValue()
        {
            var helper = NodeHelper.Create("<Application Number=\"123\" />");

            Assert.AreEqual(123, helper.GetIntAttribute("Number"));
        }

        [TestMethod]
        [ExpectedException(typeof(MagenocoException))]
        public void GetIntAttribute_InInvalidFormat_Throws()
        {
            var helper = NodeHelper.Create("<Application Number=\"fuimnota#\" />");

            helper.GetIntAttribute("Number");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NodeProcessorCtor_NullNode_Throws()
        {
            var helper = NodeHelper.CreateWithNullXml(TemplateTests.CreateTemplateSource());
        }

        [TestMethod]
        public void GetStringAttribute_TokenDefinedOnClass_ReturnsValue()
        {
            var helper = NodeHelper.Create("<Foo/>");

            Assert.AreEqual("Yay", helper.GetAttribute(new TextToken("Interjection")));
        }

        [TestMethod]
        public void GetIntAttribute_TokenDefinedOnClassAsString_ReturnsValue()
        {
            var helper = NodeHelper.Create("<Foo/>");

            Assert.AreEqual(123, helper.GetIntAttribute("StringThatLooksLikeInt"));
        }

        [TestMethod]
        public void GetIntAttribute_TokenDefinedOnClassAsInt_ReturnsValue()
        {
            var helper = NodeHelper.Create("<Foo/>");

            Assert.AreEqual(456, helper.GetIntAttribute("IntProperty"));
        }
    }
}
