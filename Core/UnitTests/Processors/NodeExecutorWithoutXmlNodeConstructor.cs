﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Magenoco.Core.UnitTests
{
    // This one isn't valid because it doesn't have a public constructor with just one XmlNode parameter.
    public class NodeExecutorWithoutXmlNodeConstructor
    {
        protected NodeExecutorWithoutXmlNodeConstructor(XmlNode node)
        {
        }

        public NodeExecutorWithoutXmlNodeConstructor(string x, string y)
        {
        }
    }
}
