﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using Magenoco.Core.Processor;
using Magenoco.Core;

namespace Magenoco.Core.UnitTests
{
    public class SimpleTestProcessor : NodeExecutor
    {
        public static int ExecuteCallCount { get; private set; }

        public SimpleTestProcessor(ITokenSource node, ITemplateSource templateSource) : base(node, templateSource)
        {
            ExecuteCallCount = 0;
        }

        public override void Execute()
        {
            ExecuteCallCount++;
        }
    }
}
