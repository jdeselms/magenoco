﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Magenoco.Core.IO;
using Magenoco.Core;
using Magenoco;

namespace Magenoco.Core.UnitTests
{
    [TestClass]
    public class ProgramTests
    {
        [TestMethod]
        public void ValidCommandLine()
        {
            var commandLine = new CommandLineArguments(new [] { "foo.xml", "bar.xml" });

            Assert.AreEqual("foo.xml", Path.GetFileName(commandLine.ProcessorFile));
            Assert.AreEqual("bar.xml", Path.GetFileName(commandLine.ApplicationFile));
        }
    }
}
