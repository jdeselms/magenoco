﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Magenoco.Core.IO;

namespace Magenoco.Core.UnitTests
{
    [TestClass]
    public class ReadFileSystemTests
    {
        private readonly string _testFileName;

        public ReadFileSystemTests()
        {
            _testFileName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "magenoco.testfile");
        }

        [TestInitialize]
        public void Setup()
        {
            Cleanup();
        }

        [TestCleanup]
        public void Cleanup()
        {
            if (File.Exists(_testFileName))
            {
                File.Delete(_testFileName);
            }
        }

        [TestMethod]
        public void ReadFile_FileExists_ReadsFile()
        {
            WriteTestFile();

            IFileSystem fileSystem = new RealFileSystem();
            Assert.AreEqual("hi", fileSystem.ReadFile(_testFileName));
        }

        [TestMethod]
        [ExpectedException(typeof(FileNotFoundException))]
        public void ReadFile_FileDoesntExist_Throws()
        {
            IFileSystem fileSystem = new RealFileSystem();
            fileSystem.ReadFile("bogus");
        }

        [TestMethod]
        public void FileExists_FileExists_True()
        {
            WriteTestFile();
            IFileSystem fileSystem = new RealFileSystem();

            Assert.IsTrue(fileSystem.FileExists(_testFileName));
        }

        [TestMethod]
        public void FileExists_FileDoesntExist_False()
        {
            IFileSystem fileSystem = new RealFileSystem();
            Assert.IsFalse(fileSystem.FileExists("bogus"));
        }

        [TestMethod]
        public void DeleteFile_FileExists_FileDeleted()
        {
            WriteTestFile();

            IFileSystem fileSystem = new RealFileSystem();
            fileSystem.DeleteFile(_testFileName);

            Assert.IsFalse(File.Exists(_testFileName));
        }

        [TestMethod]
        public void DeleteFile_FileDoesntExist_NoError()
        {
            IFileSystem fileSystem = new RealFileSystem();
            fileSystem.DeleteFile("bogus");
        }

        [TestMethod]
        public void FileExists_WhenCaseDoesntMatch_True()
        {
            WriteTestFile();

            IFileSystem fileSystem = new RealFileSystem();
            Assert.IsTrue(fileSystem.FileExists(_testFileName.ToUpper()));
        }

        private void WriteTestFile()
        {
            File.WriteAllText(_testFileName, "hi");
        }
    }
}
