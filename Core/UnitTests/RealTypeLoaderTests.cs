﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Magenoco.Core.IO;
using Magenoco.Core.Types;
using Magenoco.Core;

namespace Magenoco.Core.UnitTests
{
    [TestClass]
    public class RealTypeLoaderTests
    {
        [TestMethod]
        public void LoadType_ThatExistsInSameAssembly_Success()
        {
            ITypeLoader typeLoader = new RealTypeLoader(AppDomain.CurrentDomain.BaseDirectory);
            var type = typeLoader.LoadType("Magenoco.Core.UnitTests.dll,Magenoco.Core.UnitTests.SimpleTestProcessor");

            Assert.AreEqual("Magenoco.Core.UnitTests.SimpleTestProcessor", type.FullName);
        }

        [TestMethod]
        public void LoadType_ThatExistsThoughStringsAreNotTrimmed_Success()
        {
            ITypeLoader typeLoader = new RealTypeLoader(AppDomain.CurrentDomain.BaseDirectory);
            var type = typeLoader.LoadType(" Magenoco.Core.UnitTests.dll , Magenoco.Core.UnitTests.SimpleTestProcessor ");

            Assert.AreEqual("Magenoco.Core.UnitTests.SimpleTestProcessor", type.FullName);
        }

        [TestMethod]
        [ExpectedException(typeof(MagenocoException))]
        public void LoadType_WhenAssemblyDoesntExist_Throws()
        {
            ITypeLoader typeLoader = new RealTypeLoader(AppDomain.CurrentDomain.BaseDirectory);
            var type = typeLoader.LoadType("BogusAssembly.dll,Magenoco.UnitTests.Core.SimpleTestProcessor");
        }

        [TestMethod]
        [ExpectedException(typeof(MagenocoException))]
        public void LoadType_WhenTypeDoesntExist_Throws()
        {
            ITypeLoader typeLoader = new RealTypeLoader(AppDomain.CurrentDomain.BaseDirectory);
            var type = typeLoader.LoadType("Magenoco.UnitTests.dll,Magenoco.Core.UnitTests.BogusTestProcessor");
        }

        [TestMethod]
        [ExpectedException(typeof(MagenocoException))]
        public void LoadType_WhenTypeSpecHasNoComma_Throws()
        {
            ITypeLoader typeLoader = new RealTypeLoader(AppDomain.CurrentDomain.BaseDirectory);
            var type = typeLoader.LoadType("NoComma");
        }

        [TestMethod]
        [ExpectedException(typeof(MagenocoException))]
        public void LoadType_WhenAssemblyBlank_Throws()
        {
            ITypeLoader typeLoader = new RealTypeLoader(AppDomain.CurrentDomain.BaseDirectory);
            var type = typeLoader.LoadType(",Type");
        }

        [TestMethod]
        [ExpectedException(typeof(MagenocoException))]
        public void LoadType_WhenTypeBlank_Throws()
        {
            ITypeLoader typeLoader = new RealTypeLoader(AppDomain.CurrentDomain.BaseDirectory);
            var type = typeLoader.LoadType("Assembly,");
        }
    }
}
