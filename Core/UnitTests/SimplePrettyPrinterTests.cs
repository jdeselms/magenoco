﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Magenoco.Core.Text;

namespace Magenoco.Core.UnitTests
{
    [TestClass]
    public class SimplePrettyPrinterTests
    {
        [TestMethod]
        public void PrettyPrint_SimpleExample_NoChange()
        {
            Assert.AreEqual("SimpleTest", PrettyPrint("SimpleTest"));
        }

        [TestMethod]
        public void PrettyPrint_SimpleMultipleLines_NoChange()
        {
            Assert.AreEqual("A\r\nB", PrettyPrint("A\r\nB"));
        }

        [TestMethod]
        public void PrettyPrint_CurlyBrace_NewLineAndIndent()
        {
            Assert.AreEqual("hello\r\n{\r\n    goodbye\r\n}", PrettyPrint("hello { goodbye }"));
        }

        [TestMethod]
        public void PrettyPrint_Paren_JustIndent()
        {
            Assert.AreEqual("Function(\r\n    a,\r\n    b);\r\nBye();\r\nBye();", PrettyPrint("Function(\r\na,\r\nb);\r\n  Bye();\r\n    Bye();"));
        }

        [TestMethod]
        public void PrettyPrint_SeveralBlankLines_JustReturnOne()
        {
            Assert.AreEqual("hi\r\n\r\nbye", PrettyPrint("hi\r\n   \r\n  \r\n  \r\nbye"));
        }

        [TestMethod]
        public void PrettyPrint_BlankLineAfterCurlyBrace_RemoveIt()
        {
            Assert.AreEqual("{\r\n    hi", PrettyPrint("{\r\n\r\n\r\nhi"));
        }

        [TestMethod]
        public void PrettyPrint_BlankLineBeforeCurlyBrace_RemoveIt()
        {
            Assert.AreEqual("{\r\n    hi\r\n}", PrettyPrint("{\r\nhi\r\n\r\n\r\n}"));
        }

        [TestMethod]
        public void PrettyPrint_IntentionalBlankLine_IsPreserved()
        {
            string code = "Hello\r\n\r\nThere";

            Assert.AreEqual(code, PrettyPrint(code));
        }

        [TestMethod]
        public void PrettyPrint_NoBlankLineBeforeCurlyBrace_StillNoBlankLine()
        {
            string code = "Hello\r\n  {\r\n    there";

            Assert.AreEqual("Hello\r\n{\r\n    there", PrettyPrint(code));
        }

        private string PrettyPrint(string input)
        {
            var prettyPrinter = new CSharpPrettyPrinter();
            return prettyPrinter.Prettify(input);
        }
    }
}
