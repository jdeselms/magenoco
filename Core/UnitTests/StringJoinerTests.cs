﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Magenoco.Core.Text;

namespace Magenoco.Core.UnitTests
{
    [TestClass]
    public class StringJoinerTests
    {
        [TestMethod]
        public void Join_NoDelimiter()
        {
            string result = StringJoiner.Join(new string[] {"Hello", "There", "World"}, String.Empty);

            Assert.AreEqual("HelloThereWorld", result);
        }

        [TestMethod]
        public void Join_StringsEndWithNewLineButExplicitDelimeter_DoesntTrimLines()
        {
            string result = StringJoiner.Join(new string[] {"Hello\r", "There\r", "World\n\r\n"}, string.Empty);

            Assert.AreEqual("Hello\rThere\rWorld\n\r\n", result);
        }

        [TestMethod]
        public void Join_DelimitedNotSpecifiedAndNoNewLines_NoNewLines()
        {
            string result = StringJoiner.Join(new string[] {"Hello", "There", "World"});

            Assert.AreEqual("HelloThereWorld", result);
        }

        [TestMethod]
        public void Join_DelimitedNotSpecifiedAndEndsWithNewLines_NewLines()
        {
            string result = StringJoiner.Join(new string[] {"Hello\n", "There\r", "World\r\n"});

            Assert.AreEqual("Hello\r\nThere\r\nWorld", result);
        }

        [TestMethod]
        public void JoinWithOptionalPrefix_MoreThanOne_IncludesPrefix()
        {
            string result = StringJoiner.JoinWithPrefixIfNotEmpty(" : ", new string[] { "Foo", "Bar" }, ", ");

            Assert.AreEqual(" : Foo, Bar", result);
        }

        [TestMethod]
        public void JoinWithPrefixAndSuffix_MoreThanOne_IncludesPrefixAndSuffix()
        {
            string result = StringJoiner.JoinWithPrefixAndSuffixIfNotEmpty("(", ")", new string[] { "Foo", "Bar" }, ", ");

            Assert.AreEqual("(Foo, Bar)", result);
        }

        [TestMethod]
        public void JoinWithOptionalPrefix_None_ReturnNothing()
        {
            string result = StringJoiner.JoinWithPrefixIfNotEmpty(" : ", new string[] { }, ", ");

            Assert.AreEqual("", result);
        }

        [TestMethod]
        public void JoinWithOptionalPrefixAndSuffix_None_ReturnNothing()
        {
            string result = StringJoiner.JoinWithPrefixAndSuffixIfNotEmpty("(", ")", new string[] { }, ", ");

            Assert.AreEqual("", result);
        }
    }
}
