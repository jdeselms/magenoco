﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Magenoco.Core.Processor;
using Magenoco.Core;
using Magenoco.Core.IO;
using Magenoco.Core.Text;

namespace Magenoco.Core.UnitTests
{
    /// <summary>
    /// The processor file is the file that defines the processors that we'll be using to build the application.
    /// </summary>
    [TestClass]
    public class SystemProcessorTests
    {
        [TestMethod]
        public void Execute_WhenProcessorFileIsValid_Success()
        {
            var processor = CreateProcessor(typeof (SimpleTestProcessor));

            processor.Execute();

            Assert.AreEqual(1, SimpleTestProcessor.ExecuteCallCount);
        }

        [TestMethod]
        [ExpectedException(typeof(MagenocoException))]
        public void Execute_ProcessorIsNotCorrectType_Throws()
        {
            // This isn't the right type, so it should fail.
            var processor = CreateProcessor(typeof(MockTypeLoader));

            processor.Execute();
        }

        [TestMethod]
        [ExpectedException(typeof(MagenocoException))]
        public void Execute_ProcessorIsDoesntHaveCorrectConstructor_Throws()
        {
            // This isn't the right type, so it should fail.
            var processor = CreateProcessor(typeof(NodeExecutorWithoutXmlNodeConstructor));

            processor.Execute();
        }

        [TestMethod]
        public void CreateBuilderFromFiles_FilesExist_Success()
        {
            var fileSystem = new MockFileSystem();
            fileSystem.WriteFile(@"c:\processors.xml", "<System><Processor Code=\"MyAsm,TestClass.Processor\" /></System>");
            fileSystem.WriteFile(@"c:\application.xml", "<Application></Application>");

            MockTypeLoader typeLoader = new MockTypeLoader();
            typeLoader.AddType("MyAsm,TestClass.Processor", typeof(SimpleTestProcessor));

            var applicationXml = NodeHelper.CreateDocument("<Application></Application>");
            ApplicationBuilder proc = ApplicationBuilder.CreateBuilderFromFiles(@"c:\processors.xml", @"c:\application.xml", fileSystem, typeLoader);

            proc.Execute();

            Assert.AreEqual(1, SimpleTestProcessor.ExecuteCallCount);


        }

        private ApplicationBuilder CreateProcessor(Type processorType)
        {
            var systemXml = SimpleTokenSource.CreateFromXml("<System><Processor Code=\"MyAsm,TestClass.Processor\" /></System>");

            MockTypeLoader typeLoader = new MockTypeLoader();
            typeLoader.AddType("MyAsm,TestClass.Processor", processorType);

            var applicationXml = SimpleTokenSource.CreateFromXml("<Application></Application>");
            ApplicationBuilder proc = new ApplicationBuilder(systemXml, applicationXml, TemplateTests.CreateTemplateSource(), typeLoader: typeLoader);

            return proc;
        }
    }
}
