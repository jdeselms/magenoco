﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Magenoco.Core.Text;
using Magenoco.Core.IO;

namespace Magenoco.Core.UnitTests
{
    [TestClass]
    public class TemplateParserTests
    {
        [TestMethod]
        public void SingleTemplate()
        {
            var templates = TemplateParser.Parse("template Foo(Entity)").ToArray();
            Assert.AreEqual(1, templates.Length);
            Assert.AreEqual("Foo", templates[0].Item1);
        }

        [TestMethod]
        public void TwoTemplates()
        {
            var templates = TemplateParser.Parse("template Foo(Entity) \r\ntemplate Bar(Entity)").ToArray();
            Assert.AreEqual(2, templates.Length);
            Assert.AreEqual("Foo", templates[0].Item1);
            Assert.AreEqual("Bar", templates[1].Item1);
        }

        [TestMethod]
        public void AppliesToTest()
        {
            var templates = TemplateParser.Parse("template Foo(Bar)\r\n\r\nEND").ToArray();
            Assert.AreEqual("Bar", templates[0].Item2.AppliesTo[0]);
        }

        [TestMethod]
        public void TemplateOnSeveralLines()
        {
            var templates = TemplateParser.Parse("template Foo(Entity) \nHow are you?\ntemplate Bar(Entity)\nGood").ToArray();
            Assert.AreEqual("How are you?", templates[0].Item2.Replace(null, null));
        }

        [TestMethod]
        public void TemplateWithEnd()
        {
            var templates = TemplateParser.Parse("template Foo(Entity)\nHi\nThere\nEND\n\ntemplate Bar(Entity)\nHowdy\nEND").ToArray();
            Assert.AreEqual("Hi\nThere", templates[0].Item2.Replace(null, null));
            Assert.AreEqual("Howdy", templates[1].Item2.Replace(null, null));
        }

        [TestMethod]
        public void GetOutputFileNameOrNull_SimpleFileNameSpecified_ReturnsFileName()
        {
            // This template should be written out to temp.txt.
            var templates = TemplateParser.Parse("template Foo(Entity) temp.txt\nHi\nEND").ToArray();
            Assert.AreEqual("temp.txt", templates[0].Item2.GetOutputFileNameOrNull(null, null, null));
        }

        [TestMethod]
        public void GetOutputFileNameOrNull_NoFileNameSpecified_FileNameNull()
        {
            // This template does not write out to a file.
            var templates = TemplateParser.Parse("template Foo(Entity) \nHi\nEND").ToArray();
            Assert.AreEqual(null, templates[0].Item2.GetOutputFileNameOrNull(null, null, null));
        }

        [TestMethod]
        public void GetOutputFileNameOrNull_FileNameWithTokenSpecified_DoesSubstitution()
        {
            // Given a template with a token specified
            var templates = TemplateParser.Parse("template Foo(Entity) <<Type>>.cs").ToArray();

            var simpleTokenSource = new SimpleTokenSource("foo");
            simpleTokenSource.AddTextToken("Type", "Howdy");

            // The filename has the token substitution.
            Assert.AreEqual("Howdy.cs", templates[0].Item2.GetOutputFileNameOrNull(simpleTokenSource, null, null));
        }

        [TestMethod]
        public void Replace_IfFileSpecified_WritesToFile()
        {
            // Given a template that writes to an output file
            var templates = TemplateParser.Parse("template Foo(Entity) temp.txt\nHi\nEND").ToArray();
            MockFileSystem fileSystem = new MockFileSystem();

            // When we do the replacement,
            templates[0].Item2.Replace(null, null, fileSystem);

            // It'll actually write a file out.
            var contents = fileSystem.ReadFile("temp.txt");
            Assert.AreEqual("Hi", contents);
        }
    }
}
