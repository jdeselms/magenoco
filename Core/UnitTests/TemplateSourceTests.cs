﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Magenoco.Core.Text;
using Magenoco.Core.Processor;
using Magenoco.Core.IO;

namespace Magenoco.Core.UnitTests
{
    [TestClass]
    public class TemplateSourceTests
    {
        [TestMethod]
        public void GetTemplate_ExistsInAssembly_Success()
        {
            TemplateSource templateSource = new TemplateSource();
            templateSource.AddAssembly(Assembly.GetExecutingAssembly());

            Template template = templateSource.GetTemplate("Magenoco.Core.UnitTests.ResourceTestFile.template");
            Assert.AreEqual("This is a test.", template.Replace(null, templateSource).Trim());
        }

        [TestMethod]
        public void GetTemplate_LoadByPartialName_Success()
        {
            TemplateSource templateSource = new TemplateSource();
            templateSource.AddAssembly(Assembly.GetExecutingAssembly());

            Template template = templateSource.GetTemplate("ResourceTestFile");
            Assert.AreEqual("This is a test.", template.Replace(null, templateSource).Trim());
        }

        [TestMethod]
        [ExpectedException(typeof(MagenocoException))]
        public void GetTemplate_ResourceDoesntExist_Throws()
        {
            TemplateSource templateSource = new TemplateSource();
            templateSource.AddAssembly(Assembly.GetExecutingAssembly());

            Template template = templateSource.GetTemplate("Test");
        }
    }
}
