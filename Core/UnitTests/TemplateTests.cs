﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Magenoco.Core.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Magenoco.Core.Text;
using Magenoco.Core;
using System.Reflection;
using Magenoco.Core.Processor;

namespace Magenoco.Core.UnitTests
{
    [TestClass]
    public class TemplateTests
    {
        public static ITemplateSource CreateTemplateSource()
        {
            TemplateSource templateSource = new TemplateSource();
            templateSource.AddAssembly(Assembly.GetExecutingAssembly());
            return templateSource;
        }

        [TestMethod]
        public void TemplateWithNoTokens()
        {
            Template template = new Template("_", "", "");

            Assert.AreEqual(0, template.Tokens.Length);
        }

        [TestMethod]
        public void TemplateWithSeveralTemplates()
        {
            Template template = new Template("_", "", "<<Greeting>>, <<Name>>, how are you today<<Punctuation>>");
            
            Assert.AreEqual(3, template.Tokens.Length);
            Assert.AreEqual("Greeting", template.Tokens[0].Text);
            Assert.AreEqual("Name", template.Tokens[1].Text);
            Assert.AreEqual("Punctuation", template.Tokens[2].Text);
        }

        [TestMethod]
        public void ReplacingTokens()
        {
            Template template = new Template("_", "", "<<Greeting>>, <<Name>>!");

            SimpleTokenSource tokenSource = new SimpleTokenSource("foo");
            tokenSource.AddTextToken("Greeting", "Hello");
            tokenSource.AddTextToken("Name", "Jim");

            Assert.AreEqual("Hello, Jim!", template.Replace(tokenSource, CreateTemplateSource()));
        }

        [TestMethod]
        public void EscapedTokenTest()
        {
            Template template = new Template("_", "", "Hello, \\<<Person>>!");
            var tokenSource = new SimpleTokenSource("foo");

            Assert.AreEqual("Hello, <<Person>>!", template.Replace(tokenSource, CreateTemplateSource()));
        }

        [TestMethod]
        public void HasThreeTokens()
        {
            Template template = new Template("_", "", "public <<Type>> <<Name>> <<Name.Camel>>");
            Assert.AreEqual(3, template.Tokens.Length);
        }

        [TestMethod]
        public void TemplateWithDuplicateTokensOnlyCountsEachTokenOnce()
        {
            Template template = new Template("_", "", "<<Hello>> <<Hello>>");
            Assert.AreEqual(1, template.Tokens.Length);
            Assert.AreEqual("Hello", template.Tokens[0].Text);
        }

        [TestMethod]
        public void TokenWithTransformation()
        {
            var tokens = new Template("_", "", "<<Name.Uppercase>>").Tokens;
            Assert.AreEqual("Name", tokens[0].Text);
            Assert.AreEqual("Uppercase", tokens[0].TransformationOrNull);
        }

        [TestMethod]
        public void ChildToken_SimpleFormat_HasAllTheParts()
        {
            Template template = new Template("_", "", "{{Child}}");
            ChildToken token = template.ChildTokens[0];
            Assert.AreEqual("Child", token.TemplateName);
        }

        [TestMethod]
        public void ChildToken_TwoChildTokens_ReturnsAll()
        {
            Template template = new Template("_", "", "{{Child1}}{{Child2}}");
            Assert.AreEqual(2, template.ChildTokens.Length);
        }

        [TestMethod]
        public void TokenWithDigitsAndUnderscoresIsValid()
        {
            AssertIsValidToken("<<x1239324_2837457>>");
        }

        [TestMethod]
        public void TokenStartingWithDigitIsInvalid()
        {
            AssertIsNotValidToken("<<1239324_2837457>>");
        }

        [TestMethod]
        public void TokenStartingWithUnderscoreIsValid()
        {
            AssertIsValidToken("<<_OCEGUF>>");
        }

        [TestMethod]
        public void TokenithSpaceIsNotValid()
        {
            AssertIsNotValidToken("<<Toke n>>");
        }

        [TestMethod]
        public void TokenWithSpaceIsNotValid()
        {
            AssertIsNotValidToken("<<Toke n>>");
        }

        [TestMethod]
        public void TokenWithNoTextIsNotValid()
        {
            AssertIsNotValidToken("<<>>");
        }

        [TestMethod]
        public void AppliesToTest()
        {
            Template t = new Template("foo", "bar", "hello");
            Assert.AreEqual("bar", t.AppliesTo[0]);
        }

        [TestMethod]
        public void FromResource_NamedResource_ReturnsTemplate()
        {
            TemplateSource templateSource = new TemplateSource();
            templateSource.AddAssembly(this.GetType().Assembly);
            templateSource.GetTemplate("Magenoco.Core.UnitTests.ResourceTestFile.template");
        }

        [TestMethod]
        public void ReplacingChildTokens()
        {
            Template template = new Template("Person", "", "Hello, {{PersonName}}!");

            SimpleTokenSource tokenSource = new SimpleTokenSource("foo");
            tokenSource.AddTextToken("Greeting", "Hello");
            tokenSource.AddTextToken("Name", "Jim");

            SimpleTokenSource jim = new SimpleTokenSource("PersonNode");
            jim.AddTextToken("Name", "Jim");
            tokenSource.AddChild(jim);

            SimpleTokenSource susan = new SimpleTokenSource("PersonNode");
            susan.AddTextToken("Name", "Susan");
            tokenSource.AddChild(susan);

            TemplateSource templateSource = new TemplateSource();
            templateSource.AddTemplate("Person.PersonName", new Template("Person.PersonName", "PersonNode", "<<Name>>"));

            Assert.AreEqual("Hello, JimSusan!", template.Replace(tokenSource, templateSource));
        }

        [TestMethod]
        public void ChildTokenWithJustPropertyNameIsValid()
        {
            AssertIsValidChildToken("{{Hello}}");
        }

        [TestMethod]
        public void ChildTokenWithPropertyNameAndTemplateNameIsValid()
        {
            AssertIsValidChildToken("{{Hello}}");
        }

        [TestMethod]
        public void ChildTokenWithPropertyNameAndCodeGenNameIsValid()
        {
            AssertIsValidChildToken("{{Hello}}");
        }

        private void AssertIsValidToken(string token)
        {
            Template template = new Template("_", "", token);

            Assert.AreEqual(1, template.Tokens.Length, token + " is not a valid token");
        }

        private void AssertIsNotValidToken(string token)
        {
            Template template = new Template("_", "", token);

            Assert.AreEqual(0, template.Tokens.Length, token + " is a valid token, but expected it not to be.");
        }

        private void AssertIsValidChildToken(string childToken)
        {
            Template template = new Template("_", "", childToken);

            Assert.AreEqual(1, template.ChildTokens.Length, childToken + " is not a valid child token");
        }

        private void AssertIsNotValidChildToken(string childToken)
        {
            Template template = new Template("_", "", childToken);

            Assert.AreEqual(0, template.ChildTokens.Length, childToken + " is a valid child token, but expected it not to be.");
        }
    }
}
