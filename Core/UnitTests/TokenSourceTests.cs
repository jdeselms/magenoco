﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Threading.Tasks;
using Magenoco.Core.Text;
using Magenoco.Core.Processor;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Magenoco.Core.UnitTests
{
    [TestClass]
    public class TokenSourceTests
    {
        [TestMethod]
        public void GetTextToken_TokenExists_ReturnsToken()
        {
            var tokenSource = new SimpleTokenSource("foo");
            tokenSource.AddTextToken("Name", "Jim");

            Assert.AreEqual("Jim", tokenSource.GetAttribute(new TextToken("Name")));
        }

        [TestMethod]
        [ExpectedException(typeof(MagenocoException))]
        public void GetTextToken_TokenDoesntExist_Throws()
        {
            var tokenSource = new SimpleTokenSource("foo");

            tokenSource.GetAttribute(new TextToken("Foo"));
        }

        [TestMethod]
        public void GetTextToken_CaseInsensitiveSearch_ReturnsToken()
        {
            var tokenSource = new SimpleTokenSource("foo");
            tokenSource.AddTextToken("Name", "Jim");

            Assert.AreEqual("Jim", tokenSource.GetAttribute(new TextToken("NAME")));
        }

        [TestMethod]
        public void GetChildTokens_TokenExists_ReturnsAllChildren()
        {
            // Given a token source with a couple children
            var tokenSource = new SimpleTokenSource("foo");

            var child1 = new SimpleTokenSource("Child");
            child1.AddTextToken("Name", "Jim");

            var child2 = new SimpleTokenSource("Person");
            child2.AddTextToken("Name", "Susan");

            tokenSource.AddChild(child1);
            tokenSource.AddChild(child2);
            
            // I can get those children by looking up "Child".
            var children = tokenSource.GetChildren("Child").ToArray();
            Assert.AreEqual(1, children.Length);
            Assert.AreEqual("Jim", children[0].GetAttribute(new TextToken("Name")));
        }

        [TestMethod]
        public void GetChildTokens_TokenDoesntExist_ReturnsEmptyList()
        {
            // Given a token source with no children
            var tokenSource = new SimpleTokenSource("foo");

            // It has no children
            Assert.AreEqual(0, tokenSource.GetChildren("Child").Count());
        }

        [TestMethod]
        public void CreateFromXml()
        {
            var xml = NodeHelper.CreateDocument("<Entity Foo=\"Bar\"><Attribute Name=\"hi\"><Whatever /></Attribute></Entity>");
            ITokenSource tokenSource = SimpleTokenSource.CreateFromXml(xml, null);

            Assert.AreEqual(1, tokenSource.GetChildren("Attribute").Count());
            Assert.AreEqual(1, tokenSource.GetChildren("Attribute").First().GetChildren("Whatever").Count());
        }
    }
}
