﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Magenoco.Core;
using Magenoco.Core.IO;
using Magenoco.Core.Processor;

namespace DataLayerGen
{
    public class ColumnReferenceCodeGenerator : CodeGenerator
    {
        public ColumnReferenceCodeGenerator(ITokenSource node, ITemplateSource templateSource, IFileSystem fileSystem = null)
            : base(node, templateSource, fileSystem)
        {
        }

        public string PropertyName
        {
            get
            {
                return Pascal(GetAttribute("Name"));
            }
        }

    }
}
