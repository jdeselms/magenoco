﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Magenoco.Core;
using Magenoco.Core.IO;
using Magenoco.Core.Processor;
using Magenoco.Core.Text;

namespace DataLayerGen
{
    public class DomainDataMetadataCodeGenerator : CodeGenerator
    {
        public DomainDataMetadataCodeGenerator(ITokenSource node, ITemplateSource templateSource, IFileSystem fileSystem=null) : base(node, templateSource, fileSystem)
        {
        }

        public string ClassNamespace
        {
            get
            {
                return ParentOrNull.GetAttribute("Namespace");
            }
        }

        public string RowClass
        {
            get { return Class + "Row"; }
        }

        public string UnsafeClass
        {
            get { return "Unsafe" + Class; }
        }

        public string Struct
        {
            get { return Class + "Struct"; }
        }

        public string Class
        {
            get 
            { 
                string nameWithCorrectedCase = NameHelper.ChangeCase(GetAttribute("Name"), CaseOption.PascalCase);

                if (nameWithCorrectedCase.StartsWith("Sdd"))
                {
                    return nameWithCorrectedCase.Substring(3);
                }
                else
                {
                    return nameWithCorrectedCase;
                }
            }
        }
    }
}
