﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Magenoco.Core;
using Magenoco.Core.IO;
using Magenoco.Core.Processor;

namespace DataLayerGen
{
    public class LookupKeyCodeGenerator : CodeGenerator
    {
        public LookupKeyCodeGenerator(ITokenSource node, ITemplateSource templateSource, IFileSystem fileSystem = null)
            : base(node, templateSource, fileSystem)
        {
        }

        public string TypeName
        {
            get
            {
                int typeId = GetIntAttribute("dataTypeId");

                switch (typeId)
                {
                    case 52: return "int";
                    default: return "UNKNOWNTYPE";
                }
            }
        }

    }
}
