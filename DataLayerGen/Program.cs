﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

using Magenoco.Core;
using Magenoco.Core.Text;
using Magenoco.Core.Processor;

namespace DataLayerGen
{
    public class Program
    {
        static void Main(string[] args)
        {
            var builder = CreateBuilder();

            ITokenSource tokenSource = SimpleTokenSource.CreateFromXmlFile(@"..\..\Templates\Definitions.xml");
            builder.Run(tokenSource);
        }

        private static MagenocoBuilder CreateBuilder()
        {
            MagenocoBuilder builder = new MagenocoBuilder();

            builder.AddCodeGeneratorAssembly(Assembly.GetExecutingAssembly());
            builder.AddTemplatesFromFile(@"..\..\Templates\DomainData.template");

            builder.AddTemplateToRun("DD");

            return builder;
        }
    }
}