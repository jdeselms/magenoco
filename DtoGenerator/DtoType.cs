﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DtoGenerator
{
    public class DtoType
    {
        private readonly string _name;
        private readonly string _nameSpace;
        private readonly string _baseTypeOrNull;

        public DtoType(string name, string nameSpace, string baseTypeOrNull)
        {
            _name = name;
            _nameSpace = nameSpace;
            _baseTypeOrNull = baseTypeOrNull;
        }

        public string Name { get { return _name; } }

        public string Namespace { get { return _nameSpace; } }

        public DtoType GetBaseType(DtoTypeDictionary typeDict)
        {
            return typeDict.GetDto(_name).GetBaseType(typeDict);
        }
    }
}
