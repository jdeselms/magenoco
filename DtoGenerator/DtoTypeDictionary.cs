﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DtoGenerator
{
    public class DtoTypeDictionary
    {
        public static DtoTypeDictionary Instance = null;

        private readonly IDictionary<string, DtoType> _typeDict;

        public DtoTypeDictionary(IDictionary<String, DtoType> typeDict)
        {
            _typeDict = typeDict;
        }

        public TypeReference this[string name]
        {
            get 
            {
                string elementType;
                bool isArray;

                if (name.EndsWith("[]"))
                {
                    elementType = name.Substring(0, name.Length - 2);
                    isArray = true;
                }
                else
                {
                    elementType = name;
                    isArray = false;
                }

                DtoType dtoType;
                if (_typeDict.TryGetValue(elementType, out dtoType))
                {
                    return new DtoTypeReference(dtoType, isArray, this);
                }
                else
                {
                    return new SimpleTypeReference(elementType, isArray, null);
                }
            }
        }

        public DtoType GetDto(string dtoName)
        {
            return _typeDict[dtoName];
        }
    }
}
