﻿using Magenoco.Core;
using Magenoco.Core.Processor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DtoGenerator
{
    public class DtoTypeReader : NodeExecutor
    {
        public DtoTypeReader(ITokenSource node, ITemplateSource templateSource) : base(node, templateSource)
        {
        }

        public override void Execute()
        {
            Dictionary<String, DtoType> typeDict = new Dictionary<String, DtoType>();

            foreach (var node in this.GetChildren("Entity"))
            {
                string name = node.GetAttribute("Name");
                string nameSpace = node.GetAttribute("Namespace");
                string baseTypeOrNull;

                if (!node.TryGetAttribute("BaseType", out baseTypeOrNull))
                {
                    baseTypeOrNull = null;
                }

                typeDict.Add(name, new DtoType(name, nameSpace, baseTypeOrNull));
            }

            DtoTypeDictionary.Instance = new DtoTypeDictionary(typeDict);
        }
    }
}
