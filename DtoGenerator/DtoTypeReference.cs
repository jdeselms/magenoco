﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DtoGenerator
{
    public class DtoTypeReference : TypeReference
    {
        private readonly DtoType _dtoType;
        private readonly DtoTypeDictionary _dtoTypeDictionary;

        public DtoTypeReference(DtoType type, bool isArray, DtoTypeDictionary typeDict)
            : base(isArray)
        {
            _dtoType = type;
            _dtoTypeDictionary = typeDict;
        }

        public override string Name { get { return _dtoType.Name; } }

        public override string NamespaceOrNull
        {
            get { return _dtoType.Namespace; }
        }

        public override DtoType BaseDtoOrNull
        {
            get 
            {
                var baseTypeOrNull = _dtoType.GetBaseType(_dtoTypeDictionary);
                if (baseTypeOrNull == null)
                {
                    return null;
                }
                else
                {
                    return baseTypeOrNull;
                }
            }
        }
    }
}
