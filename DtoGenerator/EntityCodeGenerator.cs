﻿using Magenoco.Core;
using Magenoco.Core.IO;
using Magenoco.Core.Processor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DtoGenerator
{
    public class EntityCodeGenerator : CodeGenerator
    {
        public EntityCodeGenerator(ITokenSource node, ITemplateSource templateSource, IFileSystem fileSystem = null)
            : base(node, templateSource, fileSystem)
        {
        }

        public string DtoName { get { return GetAttribute("Name") + "Dto"; } }
    }
}
