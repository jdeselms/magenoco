﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

using Magenoco.Core;
using Magenoco.Core.Text;
using Magenoco.Core.Processor;

namespace DtoGenerator
{
    public class Program
    {
        static void Main(string[] args)
        {
            var builder = CreateBuilder();

            ITokenSource tokenSource = SimpleTokenSource.CreateFromXmlFile(@"..\..\DtoDefinitions.xml");
            builder.Run(tokenSource);
        }

        private static MagenocoBuilder CreateBuilder()
        {
            MagenocoBuilder builder = new MagenocoBuilder();

            builder.AddCodeGeneratorAssembly(Assembly.GetExecutingAssembly());
            builder.AddTemplatesFromFile(@"..\..\Templates\Classes.template");
            builder.AddTemplatesFromFile(@"..\..\Templates\Interfaces.template");

            builder.AddPreProcessor("DtoTypeReader");

            builder.AddTemplateToRun("Classes");
            builder.AddTemplateToRun("Interfaces");

            return builder;
        }
    }
}