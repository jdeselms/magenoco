﻿using Magenoco.Core;
using Magenoco.Core.IO;
using Magenoco.Core.Processor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DtoGenerator
{
    public class PropertyCodeGenerator : CodeGenerator
    {
        public PropertyCodeGenerator(ITokenSource node, ITemplateSource templateSource, IFileSystem fileSystem = null)
            : base(node, templateSource, fileSystem)
        {
        }

        public string SetDeclaration
        {
            get
            {
                if (GetBoolOrDefault("CanBeModified", false))
                {
                    return "set; ";
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        public string TypeName
        {
            get
            {
                string specifiedName = GetAttribute("Name");

                TypeReference typeRef = DtoTypeDictionary.Instance[specifiedName];

                return typeRef.Name;
            }
        }

        public virtual string DtoType
        {
            get { return TypeName; }
        }
    }
}
