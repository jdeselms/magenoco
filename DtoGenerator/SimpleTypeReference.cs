﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DtoGenerator
{
    public class SimpleTypeReference : TypeReference
    {
        private readonly string _name;
        private readonly string _namespaceOrNull;

        public SimpleTypeReference(string name, bool isArray, string namespaceOrNull = null) : base(isArray)
        {
            _name = name;
            _namespaceOrNull = namespaceOrNull;
        }

        public override string Name { get { return _name; } }

        public override string NamespaceOrNull { get { return _namespaceOrNull; } }

        public override DtoType BaseDtoOrNull
        {
            get { return null; }
        }

    }
}
