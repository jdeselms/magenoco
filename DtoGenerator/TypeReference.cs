﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DtoGenerator
{
    public abstract class TypeReference
    {
        private readonly bool _isArray;

        public TypeReference(bool isArray)
        {
            _isArray = isArray;
        }

        public abstract string Name { get; }

        public abstract string NamespaceOrNull { get; }

        public abstract DtoType BaseDtoOrNull { get; }
    }
}
