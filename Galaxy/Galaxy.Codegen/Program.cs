﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

using Magenoco.Core;
using Magenoco.Core.Text;
using Magenoco.Core.Processor;

namespace Galaxy.Codegen
{
    public class Program
    {
        static void Main(string[] args)
        {
            var builder = CreateBuilder();

            ITokenSource tokenSource = SimpleTokenSource.CreateFromXmlFile(@"..\..\Codegen\Objects.xml");
            builder.Run(tokenSource);
        }

        private static MagenocoBuilder CreateBuilder()
        {
            MagenocoBuilder builder = new MagenocoBuilder();

            builder.AddCodeGeneratorAssembly(Assembly.GetExecutingAssembly());
            builder.AddTemplatesFromFile(@"..\..\Codegen\Classes.template");

            builder.AddTemplateToRun("Classes");

            return builder;
        }
    }
}