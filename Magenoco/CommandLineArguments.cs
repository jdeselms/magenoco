﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Magenoco
{
    public class CommandLineArguments
    {
        private readonly string _processorFile;
        private readonly string _applicationFile;

        public CommandLineArguments(string[] args)
        {
            _processorFile = Path.GetFullPath(args[0]);
            _applicationFile = Path.GetFullPath(args[1]);
        }

        public string ProcessorFile { get { return _processorFile; } }
        public string ApplicationFile { get { return _applicationFile; } }
    }
}
