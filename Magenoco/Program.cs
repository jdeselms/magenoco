﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Magenoco.Core;
using Magenoco.Core.IO;
using Magenoco.Core.Types;

namespace Magenoco
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CommandLineArguments commandLineArgs = new CommandLineArguments(args);

            // Use the real stuff.
            string currentDirectory = Path.GetDirectoryName(Path.GetFullPath(commandLineArgs.ProcessorFile));

            Defaults.FileSystem = new RealFileSystem(currentDirectory);
            ITypeLoader typeLoader = new RealTypeLoader(currentDirectory);

            ProgramRunner runner = new ProgramRunner(commandLineArgs, Defaults.FileSystem, typeLoader);
            runner.Run();
        }
    }
}
