﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using Magenoco.Core.Types;
using Magenoco.Core.IO;
using Magenoco.Core.Processor;
using Magenoco.Core;

namespace Magenoco
{
    public class ProgramRunner
    {
        private readonly IFileSystem _fileSystem;
        private readonly ITypeLoader _typeLoader;
        private readonly CommandLineArguments _arguments;

        public ProgramRunner(CommandLineArguments arguments, IFileSystem fileSystem, ITypeLoader typeLoader)
        {
            _arguments = arguments;
            _fileSystem = fileSystem;
            _typeLoader = typeLoader;
        }

        public void Run()
        {
            var builder = ApplicationBuilder.CreateBuilderFromFiles(_arguments.ProcessorFile, _arguments.ApplicationFile, _fileSystem, _typeLoader);
            builder.Execute();
        }
    }
}
