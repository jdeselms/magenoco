﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Magenoco.Core;
using Magenoco.Core.IO;
using Magenoco.Core.Processor;

namespace TestApp
{
    public class MyCodeGenerator : CodeGenerator
    {
        public MyCodeGenerator(ITokenSource node, ITemplateSource templateSource, IFileSystem fileSystem=null) : base(node, templateSource, fileSystem)
        {
        }

        public string ToStars(string input)
        {
            return "".PadRight(input.Length, '*');
        }
    }
}
