﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Magenoco.Core;
using Magenoco.Core.Processor;
using Magenoco.Core.Text;

namespace TestApp
{
    public class MyExecutor : NodeExecutor
    {
        public MyExecutor(ITokenSource tokenSource, ITemplateSource templateSource) : base(tokenSource, templateSource)
        {
        }

        public override void Execute()
        {
            foreach (var entity in GetChildren("Entity"))
            {
                Console.WriteLine(entity.GetAttribute("Name"));

                foreach (var property in entity.GetChildren("Property"))
                {
                    Console.WriteLine("    " + property.GetAttribute("Name"));
                }
            }
        }
    }
}
