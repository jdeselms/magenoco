﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

using Magenoco.Core;
using Magenoco.Core.Text;
using Magenoco.Core.Processor;

namespace TestApp
{
    public class Program
    {
        static void Main(string[] args)
        {
            TemplateSource templateSource = new TemplateSource(typeof(MyCodeGenerator));
            templateSource.AddAssembly(Assembly.GetExecutingAssembly());

            string templateText = Defaults.FileSystem.ReadFile(@"..\..\Templates.template");            
            templateSource.AddTemplatesFromText(templateText);

            ITokenSource tokenSource = SimpleTokenSource.CreateFromXmlFile(@"..\..\ProgramInputs.xml");

            NodeExecutor executor = templateSource.GetExecutor("MyExecutor", tokenSource);
            executor.Execute();

            Template classesTemplate = templateSource.GetTemplate("Classes");
            classesTemplate.Replace(tokenSource, templateSource);
        }
    }
}