﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Magenoco.Core;
using Magenoco.Core.IO;
using Magenoco.Core.Processor;
using Magenoco.Core.Text;

namespace TestApp
{
    public class PropertyCodeGenerator : CodeGenerator
    {
        public PropertyCodeGenerator(ITokenSource node, ITemplateSource templateSource, IFileSystem fileSystem=null) : base(node, templateSource, fileSystem)
        {
        }

        public string CamelName 
        { 
            get 
            { 
                return GetName("Name", CaseOption.CamelCase); 
            } 
        }
    }
}
